import styled from 'styled-components/native';
import {TouchableOpacity, Dimensions} from 'react-native';

import Colors from '../../config/colors';

export const Card = styled.TouchableOpacity`
  width: ${parseInt(Dimensions.get('window').width * 0.40)}px;
  padding: 25px;
  border-width: 1px;
  border-radius: 2px;
  border-color: white;
  margin-bottom:10px;
  elevation:2;
  margin-top: 10px;
  align-items:center;
  justify-content:center;
  margin-horizontal:5px;

`;


export const CardLabel = styled.Text`
  color: ${props => props.color || Colors.textPurple};
  font-size: 14px;
  font-weight: bold;
  margin-top:20px;
`;
