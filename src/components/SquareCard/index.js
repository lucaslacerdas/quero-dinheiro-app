import React from 'react';
import {ActivityIndicator, View} from 'react-native';
import Button from '../Button';
import {
  Card, 
  CardLabel
} from './styles';
import {Icon} from 'native-base';
import Colors from '../../config/colors';

const SquareCard = ({
 IconName,
 Label,
 IconType,
  ...rest
}) => {
  return (
    <Card  
   { ...rest }
     >
       <Icon name={IconName}  style={{color:Colors.primary, fontSize:25}} type={IconType} />
       <CardLabel>{Label}</CardLabel>
    </Card>
  );
};

export default SquareCard;
