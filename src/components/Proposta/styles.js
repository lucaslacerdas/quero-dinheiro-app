import styled from 'styled-components/native';
import {TouchableOpacity, Dimensions} from 'react-native';

import Colors from '../../config/colors';

export const Card = styled.View`
  width: ${parseInt(Dimensions.get('window').width * 0.9)}px;
  padding: 10px;
  border-width: 1px;
  border-radius: 5px;
  border-color: white;
   margin-bottom:10px;
  elevation:2;
   margin-top: 10px;
`;
export const Row = styled.View`
  width: 100%;

  padding: 5px;
  flex-direction: row;
  align-items: ${props=>props.alignItems || 'center'} ;
  justify-content:  ${props=>props.justifyContent || 'space-between'};
`;

export const Column = styled.View`
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
`;

export const TextSmallGray = styled.Text`
  color: ${props => props.color || Colors.textPurple};
  font-size: 10px;
  font-weight: ${props => props.fontweight || 'normal'};
`;
export const TextMediumGray = styled.Text`
  color: ${Colors.textPurple};

  font-size: 13px;
  font-weight: ${props => props.fontweight || 'normal'};
`;

export const TextLargeGray = styled.Text`
  color: ${Colors.textPurple};
  font-size: 20px;
  font-weight: ${props => props.fontweight || 'normal'};
`;

export const LabelPrimary = styled.Text`
  color: ${Colors.primary};
  font-size: 20px;

  font-weight: bold;
`;
export const Wd40 = styled.Text`
  width: 40%;
`;

export const Wd60 = styled.Text`
  width: 60%;
`;

export const CardGrey = styled.View`
  border-color: ${Colors.greyLight};
  align-items:flex-start;
  justify-content:center;
  padding: 10px;
  border-width: 1px;
  border-radius: 5px;
  width: ${props => props.widthCard || 100}%;
`;
