import React from 'react';
import {ActivityIndicator, View} from 'react-native';
import Button from '../Button';
import {
  Card, 
  Row,
  TextSmallGray,
  TextMediumGray,
  TextLargeGray,
  LabelPrimary,
  Column,
  CardGrey,
  Wd40,
  Wd60,
} from './styles';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Colors from '../../config/colors';
import { verificarDadosPessoais } from '../../services/HomeService';
import {useNavigation, useIsFocused} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import HandleRequest from '../../services/HandleRequest';

// verifica se os dados obrigatorios ja foram enviados
const verificarDados = async (navigation)=>{

  // var jwtToken = AsyncStorage.getItem("@JWT_TOKEN");

  // const [handleSubmitResponse, handleSubmitError] = await HandleRequest(
  //   verificarDadosPessoais(jwtToken),
  // );

  // // apenas enquanto não houver requisição bloop
  // if (handleSubmitError) {
  //   throw new Error(
  //     'Um erro inesperado ocorreu ao consultar o cep, informe os dados de endereço manualmente',
  //   );
  // }
  navigation.navigate('ConfirmarDados');
}
const Proposta = ({
  proposta,
  valor_emprestimo,
  taxa_juros,
  parcelas,
  valor_parcela,
  situacao,
  ...rest
}) => {
  const navigation = useNavigation();
  return (
    <Card  
     >
      <Row>
        <Column>
          <TextSmallGray>PROPOSTA</TextSmallGray>
          <TextMediumGray>{proposta}</TextMediumGray>
        </Column>
        <Column>
          <TextSmallGray>Valor do Empréstimo</TextSmallGray>
          <LabelPrimary>R$ {valor_emprestimo}</LabelPrimary>
        </Column>
      </Row>
      <Row>
        
          <CardGrey widthCard={39}>
            <TextSmallGray>
              Taxa de juros de
            </TextSmallGray>
            <TextMediumGray fontweight={'bold'}>
              {taxa_juros}% ao mês
            </TextMediumGray>
          </CardGrey>
        
      
          <CardGrey widthCard={59}>
          <TextSmallGray>
              Parcelas
            </TextSmallGray>
            <TextMediumGray fontweight={'bold'}>
              {parcelas}x de R$ {valor_parcela}
            </TextMediumGray>

          </CardGrey>
      
      </Row>
      <Row justifyContent={'center'}>
        <Button style={{marginTop:20}} borderColor={Colors.themeYellow} onPress={()=>{
         verificarDados(navigation)
        }} textColor={Colors.themeYellow}>
          Contratar
        </Button>
      </Row>
    </Card>
  );
};

export default Proposta;
