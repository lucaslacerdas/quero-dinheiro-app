import styled, { css } from 'styled-components/native';
import { TouchableOpacity, Platform, Dimensions } from 'react-native';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';

import Colors from '../../config/colors';


export const Label = styled.Text`
    align-self:center;
    font-weight:bold;

   
`;
export const TextInput = styled.TextInput`
    color: ${Colors.grayTheme};
    font-family: ${Platform.OS === 'ios' ? "OpenSans" : "OpenSans-Regular"};
    width: ${parseInt(Dimensions.get('window').width * 0.75)}px;
    margin-top: 15px;
    padding: 10px;
    padding-left: 20px;
    border-color:${Colors.greenSuccess};
    border-width:1px;
    border-radius: 40px;
`;


export const Error = styled.Text`
    font-family: ${Platform.OS === 'ios' ? "OpenSans" : "OpenSans-Regular"};
    color: ${Colors.red};
    text-align:left;
    align-self:flex-start;
    font-weight:bold;
    margin-left:20px;
    margin-top:5px;
`;