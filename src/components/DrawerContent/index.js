import React, { useEffect, useMemo } from 'react';
import {Platform, TouchableOpacity, View, Text } from 'react-native';
import { DrawerItemList, DrawerItem } from '@react-navigation/drawer';
import Icon from 'react-native-vector-icons/MaterialIcons';

import { useAuth } from '../../hooks/auth';

import Colors from '../../config/colors';

import logoWhite from "../../assets/images/logo-white.png";

import {
    Container,
    ContainerHeader,
    InvisibleBlock,
    ContainerImage,
    ContainerInfo,
    ContainerAction,
    Image,
    TextCustom
} from "./styles";

export default DrawerContent = ({...props}) => {

    const { user, signOut } = useAuth()

    return (
        <Container
            {...props}
        >
            <ContainerHeader>
              
                 
            <ContainerImage>
                    
                </ContainerImage>
                <ContainerImage>
                    <Image
                        source={
                            logoWhite
                        }
                    />
                </ContainerImage>

                

                <ContainerAction>
                    <TouchableOpacity onPress={() => props.navigation.closeDrawer()}>
                        <Icon name="close" size={40} color={Colors.black}  />
                    </TouchableOpacity>
                </ContainerAction>
            </ContainerHeader>

            {/* <DrawerItemList
                {...props}
                labelStyle={{
                    fontFamily: Platform.OS === 'ios' ? "OpenSans" : "OpenSans-Regular"
                }}
                activeBackgroundColor={Colors.textPurple}

                activeTintColor={Colors.primary}
            /> */}
             <DrawerItem
                label="Início"
                labelStyle={{color:Colors.white}}
                style={{backgroundColor:Colors.purpleLight, height:80, marginBottom:-2,borderRadius:0, width:'100%', marginLeft:0, paddingLeft:20,justifyContent:'center'}}
                icon={({color, size}) => <Icon name="home" style={{ fontSize: size, color: Colors.white }} />}
                onPress={() => props.navigation.navigate('Home')}
            />
 <DrawerItem
                label="Minha Conta"
                labelStyle={{color:Colors.white}}
               style={{backgroundColor:Colors.purpleLight, height:80, marginBottom:-2,borderRadius:0, width:'100%', marginLeft:0, paddingLeft:20,justifyContent:'center'}}
                icon={({color, size}) => <Icon name="person" style={{ fontSize: size, color: Colors.white }} />}
                onPress={() => props.navigation.navigate('MeusDados')}
            />
            <DrawerItem
                label="Sair"
                labelStyle={{color:Colors.white}}
                style={{backgroundColor:Colors.purpleLight, height:80, marginBottom:-2,borderRadius:0, width:'100%', marginLeft:0, paddingLeft:20,justifyContent:'center'}}
                icon={({color, size}) => <Icon name="logout" style={{ fontSize: size, color: Colors.white }} />}
                onPress={() => signOut()}
            />
        </Container>
    );
};