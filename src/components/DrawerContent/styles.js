import styled from 'styled-components/native';
import { Platform, Dimensions } from 'react-native';
import { DrawerContentScrollView } from '@react-navigation/drawer';

import Colors from '../../config/colors';

export const Container = styled(DrawerContentScrollView)`
    background-color: ${Colors.textPurple};
    border-bottom-width: 1px;
`;

export const ContainerHeader = styled.View`
    width: 100%;
    height: 120px;

    background-color:${Colors.primary};
    padding:5px;
    flex-direction: row;
    align-items: center;
    justify-content: center;
`;

export const ContainerImage = styled.View`
    height: 100%;
    width: 33%;
    align-items: center;
    justify-content: center;
`;

export const ContainerInfo = styled.View`
    width: 60%;
    height: 100%;
    justify-content: center;
`;

export const ContainerAction = styled.View`
    height: 80%;
    width: 33%;
    align-self:flex-end;
  padding-left:20px;
    align-items:flex-end;
    justify-content:flex-start;
 

`;

export const Image = styled.Image`
    width: 80px;
    height: 80px;
    border-radius: 50px;
`;

export const TextCustom = styled.Text`
    color: ${Colors.grayTheme};
    font-family: ${Platform.OS === 'ios' ? "OpenSans" : "OpenSans-Regular"};
    margin-bottom: 2px;
    flex-wrap: wrap;
`;