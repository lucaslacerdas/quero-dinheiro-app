import styled from 'styled-components/native';
import { Platform } from 'react-native';

import Colors from '../../config/colors';
export const Container = styled.View`
    width: 50px;
    height: 50px;
    margin-top: 20px;

    border-radius: 25px;
`;

export const Input = styled.TextInput`
    flex: 1;
    color: ${Colors.gray};
    font-size: 14px;
    font-family: ${Platform.OS === 'ios' ? "OpenSans" : "OpenSans-Regular"};
    text-align: center;

    border-width: 2px;
    border-color: ${props => props.isFilled ? Colors.greenSuccess : Colors.primary};
    border-radius: 25px;
    text-transform: uppercase;
    background-color:${Colors.white};
`;