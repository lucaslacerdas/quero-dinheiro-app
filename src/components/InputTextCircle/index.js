import React, { forwardRef } from 'react';

import {
    Container,
    Input
} from './styles';

const InputTextCircle = ({ ...rest }, ref) => {

    return (
        <Container>
            <Input 
                {...rest}
                ref={ref}
            />
        </Container>
    );
};

export default forwardRef(InputTextCircle);