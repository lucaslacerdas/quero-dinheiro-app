import React from 'react';
import { ActivityIndicator } from 'react-native';

import { Container, ButtonText } from './styles';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { Colors } from 'react-native/Libraries/NewAppScreen';

const Button = ({ children, backgoundColor, borderColor, height, textColor, fontSize, fontFamily, loading, rightIconName,leftIconName,  ...rest }) => {

    return (
        <Container 
            backgoundColor={backgoundColor}
            borderColor={borderColor}
            height={height}
          
            {...rest}
        >
            {leftIconName?<Icon name={leftIconName} size={15} style={{color:Colors.white}} />:<Icon name="arrow-left" size={15} style={{color:"transparent"}} />}
            {loading ? <ActivityIndicator color={textColor} /> : (
                <ButtonText
                    textColor={textColor}
                    fontSize={fontSize}
                    fontFamily={fontFamily}
                >
                    {children}
                   
                </ButtonText>
            )}
             {rightIconName?<Icon name={rightIconName} size={15} style={{color:Colors.white, alignSelf:"center"}} />:<Icon name="user" size={15} style={{color:"transparent"}} />}
        </Container>
    );
};

export default Button;