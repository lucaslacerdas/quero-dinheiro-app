import styled from 'styled-components/native';
import { TouchableOpacity, Dimensions } from 'react-native';

import Colors from '../../config/colors';

export const Container = styled(TouchableOpacity)`
    width: ${parseInt(Dimensions.get('window').width * 0.75)}px;
    height: ${props => props.height || 49}px;
    background: ${props => props.backgoundColor || '#FFF'};

    border-color: ${props => props.borderColor || Colors.primary};
    border-width: 2px;
    border-radius: 30px;
/* 
    margin-bottom: 10px; */
    flex-direction:row;
    justify-content: space-between;
    align-items: center;
    padding-horizontal:20px;
`;

export const ButtonText = styled.Text`
    font-family: ${props => props.fontFamily || "OpenSans-Bold"};
    color: ${props => props.textColor || Colors.primary};
    font-size: ${props => props.fontSize || 16}px;
    text-align:center;
    justify-content: center;
    align-items: center;
    font-weight:bold
`;