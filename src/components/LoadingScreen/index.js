import React from 'react';
import Colors from '../../config/colors';

import { Container, Loading } from './styles';

const LoadingScreen = ({ backgroundColor = Colors.primary, loadingColor = Colors.white }) => {
    return (
        <Container style={{ backgroundColor: backgroundColor }} >
            <Loading color={loadingColor} size="large" />
        </Container>
    );
}

export default LoadingScreen;