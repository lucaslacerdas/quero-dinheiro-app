import styled from 'styled-components/native';

import Colors from '../../config/colors';

export const Container = styled.View`
    flex: 1;
    align-items: center;
    justify-content: center;

    width: 100%;
    background-color: ${Colors.primary};
`;

export const Loading = styled.ActivityIndicator`
    color: ${props => props.color || Colors.gray};
`;