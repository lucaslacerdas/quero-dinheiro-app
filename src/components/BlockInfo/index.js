// imports
import React, {useEffect, useRef} from 'react';
import {useField} from '@unform/core';
import {Block, LabelInfo,Information} from './styles';

export default function BlockInfo({Info, Label, width, ...rest}) {
  return (
    <Block  {...rest}>
      <LabelInfo>{Label}</LabelInfo>
      <Information>{Info}</Information>
    </Block>
  );
}
