
import styled, { css } from 'styled-components/native';
import {  Platform, Dimensions } from 'react-native';

import Colors from '../../config/colors';

export const LabelInfo = styled.Text`
align-self:flex-start;

font-size:11px;
color:${Colors.backgroundPurple}


`;

export const Information = styled.Text`
align-self:flex-start;

font-weight:bold;
color:${Colors.backgroundPurple}

font-size:18px;

`;

export const Block = styled.View`
margin-bottom:1px;
margin-top:10px;
padding:10px;
color: ${Colors.grayTheme};
font-family: ${Platform.OS === 'ios' ? "OpenSans" : "OpenSans-Regular"};
width: ${parseInt(Dimensions.get('window').width * 0.75)}px;

align-items:center;
justify-content:center;
border-color:${Colors.white};

border-radius: 5px;
background: rgba(233, 237, 243, 0.8)

`;
