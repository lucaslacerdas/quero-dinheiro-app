// imports
import React, {useEffect, useRef } from 'react';
import { useField } from '@unform/core';
import { TextInput, Label,Error, Container } from './styles';



export default function InputHollow( { name, label, refInput,...rest } ) {
const {fieldName, registerField, defaultValue, error } = useField(name);
const inputRef = useRef(null);
useEffect(()=>{
registerField({
    name:fieldName,
    ref:inputRef.current,
    path:'value'
})

},[fieldName,registerField]);

    return (
        <>
        <Label>{label}</Label>
        <TextInput 
        name={name}
       ref={refInput}
        {...rest}
        
        underlineColorAndroid={"transparent"}
        
        />
        {error?<Error>{error}</Error>:null}
        </>
         );
}