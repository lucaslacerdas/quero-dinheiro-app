import api from './api';

export function validarSms(dados) {
    const validaSms = new FormData();
    validaSms.append('token',dados.token);
    
    return api.post('webserviceApi/service.php', validaSms);
};