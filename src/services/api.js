import axios from 'axios';

const api = axios.create({
  // baseURL: 'https://sistema.bevicred.com.br/',
//   baseURL: "http://homologacao.bevicred.com.br/",
  baseURL: "https://producao-teste.bevicred.com.br/",
    // baseURL: 'http://192.168.33.102/',
});

export const apiNode = axios.create({
    // baseURL: "https://aplicacao.bevicred.com.br/",
    // baseURL: "http://192.168.5.110:3333/",
    // baseURL: "http://192.168.5.34:3333/",
});

export default api;