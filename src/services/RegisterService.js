import api from './api';

export function cadastraEGeraSms(dados) {
    const cadastroEGeraSms = new FormData();
    cadastroEGeraSms.append('cpf',dados.cpf);
    cadastroEGeraSms.append('senha',dados.senha);
    cadastroEGeraSms.append('nome',dados.nome);
    cadastroEGeraSms.append('celular',dados.celular);

    
    return api.post('webserviceApi/service.php', cadastroEGeraSms);
};