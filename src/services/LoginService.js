
import api from './api';

export function login(dados) {
    const loginData = new FormData();
    loginData.append('cpf',dados.cpf);
    loginData.append('senha',dados.senha);
   
    return api.post('webserviceApi/service.php', loginData);
};