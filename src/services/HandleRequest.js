export default handle = (promise) => {
    return promise
        .then(response => Promise.resolve([response.data, undefined]))
        .catch(error => Promise.resolve([undefined, error]));
};