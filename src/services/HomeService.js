import api from './api';


export function verificarDadosPessoais(dados) {
    const loginData = new FormData();
    loginData.append('cpf',dados.cpf);
    loginData.append('jwtToken',dados.jwtToken);
   
    return api.post('webserviceApi/service.php', loginData);
};