import DeviceInfo from 'react-native-device-info';

import api, { apiNode } from '../api';

const deviceId = DeviceInfo.getUniqueId();

export function login(email = '', password = '', enviaTokenEmail) {
    const smsRequest = new FormData();

    smsRequest.append('modulo', 'APPSBEVICRED');
    smsRequest.append('metodo', 'AUTHAPP');
    smsRequest.append('assunto', 'AUTHTOKENAPP');
    smsRequest.append('userlogin', email.trim());
    smsRequest.append('userpass', password.trim());
    smsRequest.append('promotora', 0);
    smsRequest.append('deviceid', deviceId);
    smsRequest.append('enviaTokenEmail', enviaTokenEmail);
    
    return api.post('webserviceApi/service.php', smsRequest);
};

export function validSMSToken(token = '', email = '', password = '') {
    const validSMSTokenResquest = new FormData();

    validSMSTokenResquest.append('modulo', 'APPSBEVICRED');
    validSMSTokenResquest.append('metodo', 'GETAPPTOKEN');
    validSMSTokenResquest.append('assunto', 'AUTHTOKENAPP');
    validSMSTokenResquest.append('userlogin', email.trim());
    validSMSTokenResquest.append('userpass', password.trim());
    validSMSTokenResquest.append('apptoken', token);
    validSMSTokenResquest.append('promotora', 0);
    validSMSTokenResquest.append('deviceid', deviceId);
    
    return api.post('webserviceApi/service.php', validSMSTokenResquest);
};

export function getUserInformation() {
    return apiNode.post('api/parceiro/dadosBasicos');
};

export function recoveryPassword(email = '') {
    const auth = new FormData();
    const token = btoa('appauthtoken:#!bevicredapp');

    auth.append('modulo', 'APPSBEVICRED');
    auth.append('metodo', 'PASSRECOVERAPP');
    auth.append('assunto', 'AUTHTOKENAPP');
    auth.append('userlogin', email.trim());
    auth.append('promotora', 0);
    // token.append('deviceid', deviceId);
    
    return api.post('webserviceApi/service.php', auth, {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',

          Accept: 'application/json',
          Authorization: 'Basic ' + token,
        }
    });
};

export const logoutApp = () => {
    return apiNode.post('/api/parceiro/logout');
};