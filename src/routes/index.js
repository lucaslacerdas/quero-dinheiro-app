import React from 'react';

//import AuthRoutes from './auth.routes';
import AppRoutes from './app.routes';

import { useAuth } from '../hooks/auth';

import LoadingScreen from '../components/LoadingScreen';

import { AlertProvider } from '../hooks/Alert';

const Routes = () => {
    const { user, isInitLoading } = useAuth();
    
    if (isInitLoading) return <LoadingScreen />
    
    //return user ? <AppRoutes/> : <AuthRoutes />;
    return <AppRoutes />
};

const RoutesProvided = () => {
    return (
        <AlertProvider>
            <Routes />
            </AlertProvider>
    );
};

export default RoutesProvided;
