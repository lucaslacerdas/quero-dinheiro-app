import React from 'react';
import { Platform, TouchableOpacity, View } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Feather from 'react-native-vector-icons/Feather';
import Colors from '../config/colors';

import Home from './../pages/Home';
import Register from './../pages/Register';
import TokenAuth from './../pages/TokenAuth';
import MeusDados from '../pages/MeusDados';
import ConfirmarDados from '../pages/ConfirmarDados';
import EnvioDocumentos from '../pages/EnvioDocumentos';
import Inicial from '../pages/Inicial';
import Login from '../pages/Login';
import ConfirmarProposta  from '../pages/ConfirmarProposta';
import ConfirmarPropostaTexto from '../pages/ConfirmarPropostaTexto';
// import ExtratoContaCorrente from './../pages/ExtratoContaCorrente';
// import SaqueContaCorrente from './../pages/SaqueContaCorrente';
// import TabelaComissionamento from './../pages/TabelaComissionamento';
// import AtualizacaoCadastral from './../pages/AtualizacaoCadastral';
// import ConviteGenerateQRCode from './../pages/ConviteGenerateQRCode';
import {Image, Text} from 'react-native'
// import LogoBevicred from './../assets/logo-bevicred-horizontal.svg';
import Logo from './../assets/images/logo.png';
import DrawerContent from '../components/DrawerContent';
import AntIcon from 'react-native-vector-icons/AntDesign';
import { useAuth } from '../hooks/auth';

const App = createStackNavigator();
const Drawer = createDrawerNavigator();


const NavigationDrawerIcon = ({ navigation }) => {

    const toggleDrawer = () => {
        navigation.toggleDrawer();
    };

    return (
        <View style={{ padding: 5}}>
            <TouchableOpacity onPress={toggleDrawer}>
                <Icon name="menu" size={30} color={Colors.textPurple} />
            </TouchableOpacity>
        </View>
    );
};

const NavigationDrawerIconBack = ({ navigation }) => {

    return (
        <View style={{ padding: 5}}>
            <TouchableOpacity onPress={()=> {navigation.goBack()}}>
                <Feather name="arrow-left" size={28} color={Colors.textPurple} />
            </TouchableOpacity>
        </View>
    );
};
const blankIcon = ({ navigation }) => {

    return (
        <View style={{ padding: 5}}>
            <TouchableOpacity onPress={null}>
                <Icon name="arrow-left" size={30} color="transparent" />
            </TouchableOpacity>
        </View>
    );
};
const UserIcon = ({ navigation }) => {
    return (
        <View style={{ padding: 5}}>
            <TouchableOpacity onPress={ ()=>{navigation.navigate('MeusDados')}}>
                <AntIcon name="user" size={30} color={Colors.textPurple} />
            </TouchableOpacity>
        </View>
    );
};



const HomeStackScreen = ({ navigation }) => {

    return (
        <App.Navigator >
            <App.Screen
                name="Home"
                component={ Home }
                options={{
                    headerLeft: () => NavigationDrawerIcon({ navigation }),
                    headerTitle: () => (
                        <View style={{ padding: 2 }} >
                            <Image source={Logo} style={{alignSelf:'center'}} width={50} height={50}/>
                        </View>
                    ),
                    headerRight: () => UserIcon({navigation}),
                    headerStyle: {
                        backgroundColor: Colors.white, 
                        elevation: 0, 
                        shadowOpacity: 0,
                        borderBottomColor:Colors.greyLight,
                        borderBottomWidth:1,
                        height:80
                    },
                    headerTintColor: Colors.textPurple,
                    cardStyle: { backgroundColor: Colors.textPurple }
                }}
            />
        </App.Navigator>
    );
};
const ConfirmarDadosStackScreen = ({ navigation }) => {

    return (
        <App.Navigator >
            <App.Screen
                name="ConfirmarDados"
                component={ ConfirmarDados }
                options={{
                    headerLeft: ()=> NavigationDrawerIconBack( {navigation}) ,
                    headerRight: () => blankIcon({ navigation }),
                    headerTitle: () => (
                        <View style={{ padding: 2, fontSize:16 }} >
                          <Text style={ { fontSize:16, fontWeight:'bold',alignSelf:'center' } } >Confirmar Dados </Text>
                        </View>
                    ),
                  
                    headerStyle: {
                        backgroundColor: Colors.white, 
                        elevation: 0, 
                        shadowOpacity: 0,
                        borderBottomColor:Colors.white,
                        borderBottomWidth:1,
                        height:80
                    },
                    headerTintColor: Colors.textPurple,
                    cardStyle: { backgroundColor: Colors.textPurple }
                }}
            />
        </App.Navigator>
    );
};
const EnviarDocumentosStackScreen = ({ navigation }) => {

    return (
        <App.Navigator >
            <App.Screen
                name="EnviarDocumentos"
                component={ EnvioDocumentos }
                options={{
                    headerLeft: ()=> NavigationDrawerIconBack( {navigation}) ,
                    headerRight: () => blankIcon({ navigation }),
                    headerTitle: () => (
                        <View style={{ padding: 2, fontSize:16 }} >
                          <Text style={ { fontSize:16, fontWeight:'bold',alignSelf:'center' } } >Envio de documentos </Text>
                        </View>
                    ),
                  
                    headerStyle: {
                        backgroundColor: Colors.white, 
                        elevation: 0, 
                        shadowOpacity: 0,
                        borderBottomColor:Colors.white,
                        borderBottomWidth:1,
                        height:80
                    },
                    headerTintColor: Colors.textPurple,
                    cardStyle: { backgroundColor: Colors.textPurple }
                }}
            />
        </App.Navigator>
    );
};
const MeusDadosStackScreen = ({ navigation }) => {
    
    return (
        <App.Navigator >
            <App.Screen
                name="Meus Dados"
                component={ MeusDados }
                options={{
                    headerLeft: () => NavigationDrawerIcon({ navigation }),
                    headerTitle: () => (
                        <View style={{ padding: 2 }} >
                            <Image source={Logo} style={{alignSelf:'center'}} width={50} height={50}/>
                        </View>
                    ),
                    headerRight: () => UserIcon({navigation}),
                    headerStyle: {
                        backgroundColor: Colors.white, 
                        elevation: 0, 
                        shadowOpacity: 0,
                        borderBottomColor:Colors.greyLight,
                        borderBottomWidth:1,
                        height:80
                    },
                    headerTintColor: Colors.textPurple,
                    cardStyle: { backgroundColor: Colors.textPurple }
                }}
            />
        </App.Navigator>
    );
};
const RegisterStackScreen = ({ navigation }) => (
    <App.Navigator >
        <App.Screen
            name="Registro"
            component={ Register }
            options={{
                headerShown: false,
                headerStyle: {backgroundColor: Colors.primary, elevation: 0, shadowOpacity: 0},
                headerTintColor: Colors.white,
                cardStyle: { backgroundColor: Colors.white }
            }}
        />
    </App.Navigator>
);

const InicialStackScreen = ({ navigation }) => (
    <App.Navigator >
        <App.Screen
            name="Início"
            component={ Inicial }
            options={{
                headerShown: false,
                headerStyle: {backgroundColor: Colors.primary, elevation: 0, shadowOpacity: 0},
                headerTintColor: Colors.white,
                cardStyle: { backgroundColor: Colors.white }
            }}
        />
    </App.Navigator>
);
const ConfirmarPropostaStackScreen = ({ navigation }) => (
    <App.Navigator >
        <App.Screen
            name="Confirmação de Proposta"
            component={ ConfirmarProposta }
            options={{
                headerLeft: ()=> NavigationDrawerIconBack( {navigation}) ,
                headerRight: () => blankIcon({ navigation }),
                headerTitle: () => (
                    <View style={{ padding: 2, fontSize:16 }} >
                      <Text style={ { fontSize:16, fontWeight:'bold',alignSelf:'center' } } >Confirmar a Proposta </Text>
                    </View>
                ),
              
                headerStyle: {
                    backgroundColor: Colors.white, 
                    elevation: 0, 
                    shadowOpacity: 0,
                    borderBottomColor:Colors.white,
                    borderBottomWidth:1,
                    height:80
                },
                headerTintColor: Colors.textPurple,
                cardStyle: { backgroundColor: Colors.textPurple }
            }}
        />
    </App.Navigator>
);
const ConfirmarPropostaTextoStackScreen = ({ navigation }) => (
    <App.Navigator >
        <App.Screen
            name="ConfirmarPropostaTexto"
            component={ ConfirmarPropostaTexto }
            options={{
                headerLeft: ()=> NavigationDrawerIconBack( {navigation}) ,
                headerRight: () => blankIcon({ navigation }),
                headerTitle: () => (
                    <View style={{ padding: 2, fontSize:16 }} >
                      <Text style={ { fontSize:16, fontWeight:'bold',alignSelf:'center' } } >Contrato da sua proposta </Text>
                    </View>
                ),
              
                headerStyle: {
                    backgroundColor: Colors.white, 
                    elevation: 0, 
                    shadowOpacity: 0,
                    borderBottomColor:Colors.white,
                    borderBottomWidth:1,
                    height:80
                },
                headerTintColor: Colors.textPurple,
                cardStyle: { backgroundColor: Colors.textPurple }
            }}
        />
    </App.Navigator>
);

const TokenAuthStackScreen = ({ navigation }) => (
    <App.Navigator >
        <App.Screen
            name="Autenticação Celular"
            component={ TokenAuth }
            options={{
                headerShown: false,
                headerStyle: {backgroundColor: Colors.primary, elevation: 0, shadowOpacity: 0},
                headerTintColor: Colors.white,
                cardStyle: { backgroundColor: Colors.white }
            }}
        />
    </App.Navigator>
);

const LoginStackScreen = ({ navigation }) => (
    <App.Navigator >
        <App.Screen
            name="Login"
            component={ Login }
            options={{
                headerShown: false,
                headerStyle: {backgroundColor: Colors.primary, elevation: 0, shadowOpacity: 0},
                headerTintColor: Colors.white,
                cardStyle: { backgroundColor: Colors.white }
            }}
        />
    </App.Navigator>
);


const AppRoutes = () => {
    const { shouldViewComponent } = useAuth();

    return (
        <>
            <Drawer.Navigator
                drawerContent={ props => <DrawerContent {...props} /> }
                drawerStyle={{width: '100%'}}
                initialRouteName="Inicial"
            >
                <Drawer.Screen
                    name="Home"
                    component={ HomeStackScreen }
                    options={{
                        drawerLabel: "Início",
                        drawerIcon: ({focused, color, size}) => (
                            <Icon name="arrow-forward-ios" style={{ fontSize: size, color: Colors.white }} />
                        ),
                    }}
                />
             
             
             <Drawer.Screen
                    name="Inicial"
                    component={ InicialStackScreen }
                    options={{
                        drawerLabel: "Início",
                        drawerIcon: ({focused, color, size}) => (
                            <Icon name="arrow-forward-ios" style={{ fontSize: size, color: Colors.white }} />
                        ),
                    }}
                />
                  <Drawer.Screen
                    name="ConfirmarProposta"
                    component={ ConfirmarPropostaStackScreen }
                    options={{
                        drawerLabel: "Início",
                        drawerIcon: ({focused, color, size}) => (
                            <Icon name="arrow-forward-ios" style={{ fontSize: size, color: Colors.white }} />
                        ),
                    }}
                />
                  <Drawer.Screen
                    name="ConfirmarPropostaTexto"
                    component={ ConfirmarPropostaTextoStackScreen }
                    options={{
                        drawerLabel: "Contrato da sua proposta",
                        drawerIcon: ({focused, color, size}) => (
                            <Icon name="arrow-forward-ios" style={{ fontSize: size, color: Colors.white }} />
                        ),
                    }}
                />
                 <Drawer.Screen
                    name="Login"
                    component={ LoginStackScreen }
                    options={{
                        drawerLabel: "Início",
                        drawerIcon: ({focused, color, size}) => (
                            <Icon name="arrow-forward-ios" style={{ fontSize: size, color: Colors.white }} />
                        ),
                    }}
                />

                  <Drawer.Screen
                    name="MeusDados"
                    component={ MeusDadosStackScreen }
                    options={{
                        drawerLabel: "Meus Dados",
                        drawerIcon: ({focused, color, size}) => (
                            <Icon name="arrow-forward-ios" style={{ fontSize: size, color: Colors.white }} />
                        ),
                    }}
                />
                   <Drawer.Screen
                    name="TokenAuth"
                    component={ TokenAuthStackScreen }
                    options={{
                       
                        drawerIcon: ({focused, color, size}) => (
                            <Icon name="arrow-forward-ios" style={{ fontSize: size, color: Colors.white }} />
                        ),
                    }}
                />
                   <Drawer.Screen
                    name="ConfirmarDados"
                    component={ ConfirmarDadosStackScreen }
                    options={{
                       
                        drawerIcon: ({focused, color, size}) => (
                            <Icon name="arrow-forward-ios" style={{ fontSize: size, color: Colors.white }} />
                        ),
                    }}
                />
                    <Drawer.Screen
                    name="EnvioDocumentos"
                    component={ EnviarDocumentosStackScreen }
                    options={{
                       
                        drawerIcon: ({focused, color, size}) => (
                            <Icon name="arrow-forward-ios" style={{ fontSize: size, color: Colors.white }} />
                        ),
                    }}
                />
                   <Drawer.Screen
                    name="Register"
                    component={ RegisterStackScreen }
                    
                    options={{
                        drawerLabel: "Registro",
                        drawerIcon: ({focused, color, size}) => (
                            <Icon name="arrow-forward-ios" style={{ fontSize: size, color: Colors.white }} />
                        ),
                    }}
                />
            </Drawer.Navigator>
        
        </>
    );
};

export default AppRoutes;
