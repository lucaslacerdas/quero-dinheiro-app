import React, {
    createContext,
    useContext,
    useState
} from 'react';

import HandleRequest from '../services/HandleRequest';
import { listCities } from '../services/Cidades';

const AtualizacaoCadastralContextData = {
    cities: [],
    selectedState: { form: '', campo: '', value: null, error: null },
    selectedCity: { form: '', campo: '', value: null, error: null },
    setCities: () => {},
    setSelectedState: () => {},
    setSelectedCity: () => {},
    getCities: () => {},
};

const AtualizacaoCadastralContext = createContext({...AtualizacaoCadastralContextData});

const AtualizacaoCadastralProvider = ({ children }) => {
    const [cities, setCities] = useState([]);
    const [selectedState, setSelectedState] = useState({ form: '', campo: '', value: null, error: null });
    const [selectedCity, setSelectedCity] = useState({ form: '', campo: '', value: null, error: null});

    // console.log('selectedState', selectedState);

    const getCities = async () => {
        try {
            const [success, error] = await HandleRequest(listCities(selectedState.value));

            if (error) {
                console.log(error);
                throw new Error('Um erro inesperado ocorreu ao tentar carregar as cidades, tente novamente mais tarde.');
            }

            if (success && success.resultSet) {
                setCities(success.resultSet);
            }
        } catch (err) {
            console.log('err', err);
        }
    };
    
    return (
        <AtualizacaoCadastralContext.Provider value={{
            cities,
            selectedState,
            selectedCity,
            setCities,
            setSelectedState,
            setSelectedCity,
            getCities
        }}>
            {children}
        </AtualizacaoCadastralContext.Provider>
    )
};

const useAtualizacaoCadastral = () => {
    const context = useContext(AtualizacaoCadastralContext);
  
    if (!context) {
      throw new Error('useAtualizacaoCadastral must be used within an AtualizacaoCadastralProvider');
    }
  
    return context;
};

export { AtualizacaoCadastralProvider, useAtualizacaoCadastral };