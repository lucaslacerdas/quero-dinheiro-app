import React, {
    createContext,
    useCallback,
    useContext,
    useState,
    useEffect,
} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';

import api, { apiNode } from '../services/api';
import { login, validSMSToken, getUserInformation, logoutApp } from '../services/Auth';
import HandleRequest from '../services/HandleRequest';

const AuthState = {
    token: '',
    tokenNode: '',
    user: null,
};

const AuthContextData = {
    user: null,
    tokenNode: '',
    token: '',
    authResponse: null,
    loginLoading: false,
    smsTokenLoading: false,
    isInitLoading: true,
    signIn: () => {},
    signOut: () => {},
    saveToken: () => {},
    setLoginLoading: () => {},
    shouldViewComponent: () => {}
};

const AuthContext = createContext({...AuthContextData});

const AuthProvider = ({ children }) => {
    const [data, setData] = useState({...AuthState});
    const [authResponse, setAuthResponse] = useState(null);
    const [loginLoading, setLoginLoading] = useState(false);
    const [smsTokenLoading, setSmsTokenLoading] = useState(false);
    const [isInitLoading, setIsInitLoading] = useState(true);

    const getuserInformatios = async (token) => {
        const [userInfo, userInfoError] = await HandleRequest(getUserInformation(token));
        
        if (userInfoError) {
            setSmsTokenLoading(false);
            console.log(userInfoError.message);
            throw new Error('Um erro inesperado ocorreu, tente novamente mais tarde.');
        }

        return userInfo.resultSet;
    };
  
    const signIn = useCallback(async ({ email, password, rememberPassword }, callbackFunction = () => {}, enviaTokenEmail = false) => {
        const [authSMS, authSMSError] = await HandleRequest(login(email, password, enviaTokenEmail));
        
        if (authSMSError) {
            setLoginLoading(false);
            throw new Error('Um erro inesperado ocorreu, tente novamente mais tarde.');
        }

        if (authSMS && authSMS.success === false) {
            setLoginLoading(false);
            throw new Error(authSMS.message || 'Um erro inesperado ocorreu, tente novamente mais tarde.');
        }

        if (authSMS.dados.jwtToken != undefined && authSMS.tokenNode != undefined) {
            setHeadersToken(authSMS.dados.jwtToken, authSMS.tokenNode);

            await AsyncStorage.setItem('@bevicred:token', authSMS.dados.jwtToken);
            await AsyncStorage.setItem('@bevicred:tokenNode', authSMS.tokenNode);

            const user = await getuserInformatios(authSMS.tokenNode);

            await AsyncStorage.setItem('@bevicred:user', JSON.stringify(user));
            
            setData({ user, token: authSMS.dados.jwtToken, tokenNode: authSMS.tokenNode });

            await AsyncStorage.setItem('@bevicred:authData', JSON.stringify({ email: email.toUpperCase(), password: rememberPassword ? btoa(password) : '' }));

            setLoginLoading(false);
        } else if (authSMS){
            setAuthResponse({...authSMS, codigoparceiro: email.toUpperCase(), password});

            await AsyncStorage.setItem('@bevicred:authData', JSON.stringify({ email: email.toUpperCase(), password: rememberPassword ? btoa(password) : '' }));

            setLoginLoading(false);
            callbackFunction();
        }
    }, [data]);

    const saveToken = useCallback(async (token = '', email, password) => {
        setSmsTokenLoading(true);
        const [validSMSTokenResponse, validSMSTokenError] = await HandleRequest(validSMSToken(token, email, password));

        if (validSMSTokenError) {
            setSmsTokenLoading(false);
            throw new Error('Um erro inesperado ocorreu, tente novamente mais tarde.');
        }

        if (validSMSTokenResponse && validSMSTokenResponse.success === false) {
            setSmsTokenLoading(false);
            throw new Error((validSMSTokenResponse.message.trim() ? validSMSTokenResponse.message : validSMSTokenResponse.dados) || 'Um erro inesperado ocorreu, tente novamente mais tarde.');
        }

        if (validSMSTokenResponse.dados && validSMSTokenResponse.tokenNode) {

            setHeadersToken(validSMSTokenResponse.dados, validSMSTokenResponse.tokenNode);

            await AsyncStorage.setItem('@bevicred:token', validSMSTokenResponse.dados);
            await AsyncStorage.setItem('@bevicred:tokenNode', validSMSTokenResponse.tokenNode);

            const user = await getuserInformatios(validSMSTokenResponse.tokenNode);

            await AsyncStorage.setItem('@bevicred:user', JSON.stringify(user));

            setData({ user, token: validSMSTokenResponse.dados, tokenNode: validSMSTokenResponse.tokenNode });
            setSmsTokenLoading(false);
        }

    }, [data]);

    const signOutSuccess = async () => {
        setHeadersToken('', '');
        
        await AsyncStorage.multiRemove(['@bevicred:user', '@bevicred:token', '@bevicred:tokenNode']);
  
        setData({...AuthState});
    };

    const signOut = useCallback(async () => {
        const [logoutAppResponse, logoutAppError] = await HandleRequest(logoutApp());

        signOutSuccess();
    }, []);

    const setHeadersToken = (token = '', tokenNode = '') => {
        if (token && tokenNode) {
            api.defaults.headers.common['Authorization'] = token;
    
            apiNode.defaults.headers.common['Authorization'] = tokenNode;

            apiNode.interceptors.response.use((response) => {
                return response
            }, async (error) => {
                
                if (error.response.status == 401) {
                    if (error.response.config.url === "/api/parceiro/logout") {
                        await signOutSuccess();
                    } else {
                        signOut();
                    }
                }
            });
            
            // apiNode.defaults.headers.common['ContentType'] = 'application/json'
        } else {
            delete api.defaults.headers.common['Authorization'];
    
            delete apiNode.defaults.headers.common['Authorization'];
        }
    };

    const shouldViewComponent = (nomeCampo = '') => {
        // const retorno = data.user.modulosLiberados.find(module => (module.nomeCampo === nomeCampo));
        
        // return retorno && retorno.ativo === 0 ? false : true;
        return true;
    };

    useEffect(() => {
        async function loadStoragedData() {
            try {
                setIsInitLoading(true);

                const [token, tokenNode, user] = await AsyncStorage.multiGet([
                    '@bevicred:token',
                    '@bevicred:tokenNode',
                    '@bevicred:user',
                ]);
    
                if (token[1] && tokenNode[1] && user[1]) {
                    setHeadersToken(token[1], tokenNode[1]);
                    const userDados = await getuserInformatios(tokenNode[1]);
                    setData({ token: token[1], tokenNode: tokenNode[1], user: !userDados ? JSON.parse(user[1]) : userDados });
                }
    
                setIsInitLoading(false);
            } catch (error) {
                setData({...AuthState});
                setIsInitLoading(false);
            }
        };

        loadStoragedData();
        // signOut();
    }, []);

    return (
        <AuthContext.Provider
            value={{
                user: data.user,
                token: data.token,
                tokenNode: data.tokenNode,
                authResponse,
                loginLoading,
                smsTokenLoading,
                isInitLoading,
                signIn,
                signOut,
                saveToken,
                setLoginLoading,
                shouldViewComponent
            }}
        >
          {children}
        </AuthContext.Provider>
    );
};

const useAuth = () => {
    const context = useContext(AuthContext);
  
    if (!context) {
      throw new Error('useAuth must be used within an AuthProvider');
    }
  
    return context;
};

export { AuthProvider, useAuth };