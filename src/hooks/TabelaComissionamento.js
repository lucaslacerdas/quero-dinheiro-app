import React, {
    useEffect,
    createContext,
    useContext,
    useState
} from 'react';

const FinanceiraSelectDefault = { codigo: null, nome: null, logoImage: null };
const ConvenioSelectDefault = { codigo: null, nomeConvenio: null };
const FormaContratoSelectDefault = { codigo: null, nomeFormaContrato: null };

const TabelaComissionamentoContextData = {
    financeiraSelected: {...FinanceiraSelectDefault},
    convenioSelected: {...ConvenioSelectDefault},
    formaContratoSelected: {...FormaContratoSelectDefault},
    setFinanceiraSelected: () => {},
    setConvenioSelected: () => {},
    setFormaContratoSelected: () => {},
};

const TabelaComissionamentoContext = createContext({...TabelaComissionamentoContextData});

const TabelaComissionamentoProvider = ({ children }) => {
    const [financeiraSelected, setFinanceiraSelected] = useState({...FinanceiraSelectDefault});
    const [convenioSelected, setConvenioSelected] = useState({...ConvenioSelectDefault});
    const [formaContratoSelected, setFormaContratoSelected] = useState({...FormaContratoSelectDefault});

    useEffect(() => {}, []);

    return (
        <TabelaComissionamentoContext.Provider
            value={{
                financeiraSelected,
                convenioSelected,
                formaContratoSelected,
                setConvenioSelected,
                setFinanceiraSelected,
                setFormaContratoSelected
            }}
        >
            {children}
        </TabelaComissionamentoContext.Provider>
    );
};

const useTabelaComissionamento = () => {
    const context = useContext(TabelaComissionamentoContext);
  
    if (!context) {
      throw new Error('useTabelaComissionamento must be used within an TabelaComissionamentoProvider');
    }
  
    return context;
};

export { TabelaComissionamentoProvider, useTabelaComissionamento };