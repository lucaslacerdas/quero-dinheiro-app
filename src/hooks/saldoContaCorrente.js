import React, {
    createContext,
    useCallback,
    useContext,
    useState,
} from 'react';

import { getSaldoContaCorrente } from '../services/ContaCorrente';
import HandleRequest from '../services/HandleRequest';

const contextData = {
    saldoContaCorrente: '',
    taxaSaqueContaCorrente: '',
    isSaldoLoading: false,
    getSaldo: () => {},
    setIsSaldoLoading: () => {}
};

const SaldoContaCorrenteContext = createContext({...contextData});

const SaldoContaCorrenteProvider = ({ children }) => {
    const [saldoContaCorrente, setSaldoContaCorrente] = useState('');
    const [taxaSaqueContaCorrente, setTaxaSaqueContaCorrente] = useState('');
    const [isSaldoLoading, setIsSaldoLoading] = useState(false);

    const getSaldo = useCallback(async() => {
        try {
            const [getSaldoContaCorrenteResponse, getSaldoContaCorrenteError] = await HandleRequest(getSaldoContaCorrente());

            if(getSaldoContaCorrenteError || !getSaldoContaCorrenteResponse.status){
                throw new Error('Um erro inesperado ocorreu, tente novamente mais tarde.');
            }
            
            setSaldoContaCorrente(getSaldoContaCorrenteResponse.resultSet.saldo);
            setTaxaSaqueContaCorrente(getSaldoContaCorrenteResponse.resultSet.resumoSaque.taxaSaque);
            setIsSaldoLoading(false);
        } catch (err) {
            console.log(err.message);
            setIsSaldoLoading(false);
        }
    }, []);

    return (
        <SaldoContaCorrenteContext.Provider 
            value={{
                saldoContaCorrente,
                taxaSaqueContaCorrente,
                isSaldoLoading,
                getSaldo,
                setIsSaldoLoading
            }}
        >
            {children}
        </SaldoContaCorrenteContext.Provider>
    );
};

const useSaldoContaCorrente = () => {
    const context = useContext(SaldoContaCorrenteContext);
  
    if (!context) {
      throw new Error('useSaldoContaCorrente must be used within an SaldoContaCorrenteContext');
    }
  
    return context;
};

export { SaldoContaCorrenteProvider, useSaldoContaCorrente };