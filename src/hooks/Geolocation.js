import React, {
    createContext,
    useCallback,
    useContext,
    useEffect,
    useState
} from 'react';
import {
    Linking,
    Platform,
    PermissionsAndroid
} from 'react-native';
import Geolocation from 'react-native-geolocation-service';

import Colors from '../config/colors';

import appConfig from '../../app.json';

import { useAlert } from '../hooks/Alert';

const GeolocationContextData = {
    isGeolocationLoading: false,
    geoCoords: { latitude: 0, longitude: 0 },
    getGeolocation: () => {},
};

const GeolocationContext = createContext({ ...GeolocationContextData });

const GeolocationProvider = ({ children }) => {
    const { showAlert, hideAlert } = useAlert();
    const [isGeolocationLoading, setIsGeolocationLoading] = useState(false);
    const [geoCoords, setGeoCoords] = useState(false);

    const hasLocationPermissionIOS = async () => {
        const openSetting = () => {
            Linking.openSettings().catch(() => {
                showAlert({
                    title: 'Ops, erro ao abrir as configurações!',
                    message: 'Verifique se seu GPS está ativo.',
                    titleStyle: { color: Colors.red, fontSize: 20 },
                    showConfirmButton: true,
                    confirmText: 'OK!',
                    confirmButtonColor: Colors.primary,
                    closeOnTouchOutside: false,
                    closeOnHardwareBackPress: false,
                    onConfirmPressed: () => hideAlert(),
                });
            });
        };

        const status = await Geolocation.requestAuthorization('whenInUse');

        if (status === 'granted') {
            return true;
        }

        if (status === 'denied') {
            showAlert({
                title: 'Ops!!!',
                message: 'Permissão negada para o Serviço de Localização.',
                titleStyle: { color: Colors.red, fontSize: 20 },
                showConfirmButton: true,
                confirmText: 'OK!',
                confirmButtonColor: Colors.primary,
                closeOnTouchOutside: false,
                closeOnHardwareBackPress: false,
                onConfirmPressed: () => hideAlert(),
            });
        }

        if (status === 'disabled') {
            showAlert({
                title: 'Ops!!!',
                message: `Por favor, ligue o serviço de localização para permitir o ${appConfig.displayName} determinar sua localização!`,
                titleStyle: { color: Colors.red, fontSize: 20 },
                showConfirmButton: true,
                showCancelButton: true,
                confirmText: 'Abrir Configurações',
                cancelText: 'Cancelar',
                confirmButtonColor: Colors.primary,
                closeOnTouchOutside: false,
                closeOnHardwareBackPress: false,
                onConfirmPressed: () => openSetting(),
                onCancelPressed: () => hideAlert(),
            });
        }

        return false;
    };

    const hasLocationPermission = async () => {
        if (Platform.OS === 'ios') {
            const hasPermission = await hasLocationPermissionIOS();
            return hasPermission;
        }

        if (Platform.OS === 'android' && Platform.Version < 23) {
            return true;
        }

        const hasPermission = await PermissionsAndroid.check(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        );

        if (hasPermission) {
            return true;
        }

        const status = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        );

        if (status === PermissionsAndroid.RESULTS.GRANTED) {
            return true;
        }

        if (status === PermissionsAndroid.RESULTS.DENIED) {
            showAlert({
                title: 'Ops!!!',
                message: 'Serviço de Localização recusado pelo usuário.',
                titleStyle: { color: Colors.red, fontSize: 20 },
                showConfirmButton: true,
                confirmText: 'OK!',
                confirmButtonColor: Colors.primary,
                closeOnTouchOutside: false,
                closeOnHardwareBackPress: false,
                onConfirmPressed: () => hideAlert(),
            });
        } else if (status === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
            showAlert({
                title: 'Ops!!!',
                message: 'Permissão revogada para o Serviço de Localização.',
                titleStyle: { color: Colors.red, fontSize: 20 },
                showConfirmButton: true,
                confirmText: 'OK!',
                confirmButtonColor: Colors.primary,
                closeOnTouchOutside: false,
                closeOnHardwareBackPress: false,
                onConfirmPressed: () => hideAlert(),
            });
        }

        return false;
    };

    const getGeolocation = useCallback(() => {
        return new Promise(async (resolve, reject) => {
            const hasLocationPermissed = await hasLocationPermission();
    
            if (!hasLocationPermissed) {
                resolve(null);
            }
    
            setIsGeolocationLoading(true);
    
            Geolocation.getCurrentPosition((position) => {
                resolve(position);
            }, (error) => {
                resolve(null);
            }, {
                enableHighAccuracy: true,
                timeout: 15000,
                maximumAge: 10000,
                distanceFilter: 0,
                forceRequestLocation: true,
                showLocationDialog: true,
            });
        });
    }, []);

    useEffect(() => {
        const init = async () => {
            const coords = await getGeolocation();

            if (coords) {
                const { latitude, longitude } = coords.coords;

                setGeoCoords({ latitude, longitude });
            }
        };

        init();
    }, []);

    return (
        <GeolocationContext.Provider value={{
            getGeolocation,
            geoCoords,
            isGeolocationLoading,
        }}>
            {children}
        </GeolocationContext.Provider>
    );
};

const useGeolocation = () => {
    const context = useContext(GeolocationContext);

    if (!context) {
        throw new Error('useGeolocation must to be used within an GeolocationProvider');
    }

    return context;
};

export { GeolocationProvider, useGeolocation };