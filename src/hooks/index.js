import React from 'react';

import { AuthProvider } from  './auth';
// import { SocketProvider } from  './Socket';
// import { GeolocationProvider } from  './Geolocation';
// import { SaldoContaCorrenteProvider } from  './saldoContaCorrente';

const AppProvider = ({ children }) => (
    // <GeolocationProvider>
        <AuthProvider>
            {/* <SocketProvider> */}
                {/* <SaldoContaCorrenteProvider> */}
                    {children}
                {/* </SaldoContaCorrenteProvider> */}
            {/* </SocketProvider> */}
        </AuthProvider>
    // </GeolocationProvider>
);

export default AppProvider;