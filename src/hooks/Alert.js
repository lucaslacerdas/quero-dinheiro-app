import React, {
    createContext,
    useContext,
    useState
} from 'react';
import AwesomeAlert from 'react-native-awesome-alerts';

const AlertContextData = {
    showAlert: () => {},
    hideAlert: () => {},
};

const AlertContext = createContext({...AlertContextData});

const AlertProvider = ({ children }) => {
    const [alertProps, setAlertProps] = useState({});

    const showAlert = ({ ...props }) => {
        console.log('called');
        setAlertProps({ ...props, show: true });
    };

    const hideAlert = (callback = () => {}) => {
        setAlertProps({ show: false });
        callback();
    };
    
    return (
        <AlertContext.Provider value={{
            showAlert,
            hideAlert,
        }}>
            {children}
            <AwesomeAlert
                {...alertProps}
                messageStyle={{
                    textAlign: 'center'
                }}
                
            />
        </AlertContext.Provider>
    )
};

const useAlert = () => {
    const context = useContext(AlertContext);
  
    if (!context) {
      throw new Error('useAlert must be used within an AlertProvider');
    }
  
    return context;
};

export { AlertProvider, useAlert };