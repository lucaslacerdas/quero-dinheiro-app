import React, {
    createContext,
    useContext,
    useState,
} from 'react';

import { useAlert } from './Alert';

import Colors from '../config/colors';

import { getPermissaoSaque } from '../services/ContaCorrente';
import HandleRequest from '../services/HandleRequest';

const contextData = {
    permissaoSaque: 'N',
    permissaoSaqueMsg: '',
    verificaPermissaoSaque: () => {}
};

const PermissaoSaqueContext = createContext({...contextData});

const PermissaoSaqueProvider = ({ children }) => {
    const { showAlert, hideAlert } = useAlert();
    const [permissaoSaque, setPermissaoSaque] = useState('N');
    const [permissaoSaqueMsg, setPermissaoSaqueMsg] = useState('');

    const verificaPermissaoSaque = async () => {
        try {
            const [verificaPermissaoSaqueResponse, verificaPermissaoSaqueError] = await HandleRequest(getPermissaoSaque());

            if(verificaPermissaoSaqueError || !verificaPermissaoSaqueResponse.status){
                throw new Error('Um erro inesperado ocorreu, tente novamente mais tarde.');
            }

            setPermissaoSaque(verificaPermissaoSaqueResponse.resultSet.podeSacar);
            setPermissaoSaqueMsg(verificaPermissaoSaqueResponse.resultSet.mensagem);

        } catch (err){
            console.log('Error: ' + JSON.stringify(err));
            showAlert({
                title: 'Ops, erro ao verificar permissão de saque!',
                message: 'Um erro inesperado ocorreu ao tentar verificar permissão de saque, tente novamente mais tarde.',
                titleStyle: { color: Colors.red, fontSize: 20 },
                showConfirmButton: true,
                confirmText: 'OK!',
                confirmButtonColor: Colors.primary,
                closeOnTouchOutside: false,
                closeOnHardwareBackPress: false,
                onConfirmPressed: () => hideAlert(),
            });
        }
    };

    return (
        <PermissaoSaqueContext.Provider 
            value={{
                permissaoSaque,
                permissaoSaqueMsg,
                verificaPermissaoSaque
            }}
        >
            {children}
        </PermissaoSaqueContext.Provider>
    );
};

const usePermissaoSaque = () => {
    const context = useContext(PermissaoSaqueContext);
  
    if (!context) {
      throw new Error('usePermissaoSaque must be used within an PermissaoSaqueContext');
    }
  
    return context;
};

export { PermissaoSaqueProvider, usePermissaoSaque };