import React, {
    createContext,
    useCallback,
    useContext,
    useState,
    useEffect,
} from 'react';
import { Alert } from 'react-native';

import HandleRequest from '../services/HandleRequest';
import { getExtratoContaCorrente } from '../services/ContaCorrente';

import { useAlert } from './Alert';


import Colors from '../config/colors';

const contextData = {
    listaHistorico: [],
    setListaHistorico: () => {},
    isRefreshing: false,
    isMessage: false,
    endLoadingList: false,
    getHistorico: async() => {},
    onRefresh: () => {},
    onEndReached: () => {},
    buttonGetHistorico: () => {}
};

const ExtratoContaCorrenteContext = createContext({...contextData});

const ExtratoContaCorrenteProvider = ({ children }) => {
    const [listaHistorico, setListaHistorico] = useState([]);
    const [waitingList, setWaitingList] = useState(false);
    const [endLoadingList, setEndLoadingList] = useState(false);
    const [isRefreshing, setIsRefreshing] = useState(false);
    const [isMessage, setIsMessage] = useState(false);
    const [periodoList, setPeriodoList] = useState(1);
    const [pageList, setPageList] = useState(1);
    const { showAlert, hideAlert } = useAlert();

    const getHistorico = async(periodo = 1, pagina = 1, initList = false) => {
        try {
            setPeriodoList(periodo);
            setPageList(pagina);
            setEndLoadingList(false);

            const [getHistoricoResponse, getHistoricoError] = await HandleRequest(getExtratoContaCorrente(periodo, pagina, 10));

            if(getHistoricoError || !getHistoricoResponse.status){
                throw new Error('Um erro inesperado ocorreu, tente novamente mais tarde.');
            }

            const dados = [];
            getHistoricoResponse.resultSet.extrato.map((item, index) => {
                if(item.tipoLancamento != ""){
                    dados.push(
                        {
                            title: item.descricao, 
                            description: item.dataLancamento, 
                            circleColor: (item.tipoLancamento == 'D') ? Colors.redTheme : Colors.greenTheme, 
                            dotColor: (item.tipoLancamento == 'D') ? Colors.redTheme : Colors.greenTheme, 
                            valor: item.valor
                        }
                    );
                }
            })

            if(dados.length > 0){
                var list = [
                    ...listaHistorico, 
                    ...dados
                ];

                if(listaHistorico.length > 0 && initList){
                    setListaHistorico(dados);
                } else {
                    setListaHistorico(list);
                }

                setIsMessage(false);
            } else {
                if(listaHistorico.length > 0 && !initList){
                    setIsMessage(false);
                    setEndLoadingList(true);
                } else {
                    setIsMessage(true);
                    setListaHistorico([]);
                }
            }
        
        } catch (err) {
            showAlert({
                title: 'Ops, erro ao listar o extrato!',
                message: 'Um erro inesperado ocorreu ao tentar listar o extrato, tente novamente mais tarde.',
                titleStyle: { color: Colors.red, fontSize: 20 },
                showConfirmButton: true,
                confirmText: 'OK!',
                confirmButtonColor: Colors.primary,
                closeOnTouchOutside: false,
                closeOnHardwareBackPress: false,
                onConfirmPressed: () => hideAlert(),
            });
        }
    };

    const onRefresh = async() => {
        setListaHistorico([]);
        setIsRefreshing(true);
        setEndLoadingList(false);
        
        //refresh to initial data
        await getHistorico(1, 1, true);

        setIsRefreshing(false);
        setWaitingList(false);
    }

    const onEndReached = () => {
        if(!waitingList && !endLoadingList){
            setPageList(pageList + 10);
            setWaitingList(true);

            setTimeout(async() => {
                //refresh to initial data
                await getHistorico(periodoList, pageList + 10);

                setIsRefreshing(false);
                setWaitingList(false);
            }, 1000);
        }
    }

    const buttonGetHistorico = async(periodo, pagina) => {
        setListaHistorico([]);
        setEndLoadingList(false);
        setIsMessage(false);

        await getHistorico(periodo, pagina, true);

        setWaitingList(false);
    }

    useEffect(() => {
        getHistorico(1, 1, true);
    }, []);

    return (
        <ExtratoContaCorrenteContext.Provider 
            value={{
                listaHistorico,
                setListaHistorico,
                isRefreshing,
                isMessage,
                endLoadingList,
                getHistorico,
                onRefresh,
                onEndReached,
                buttonGetHistorico
            }}
        >
            {children}
        </ExtratoContaCorrenteContext.Provider>
    );
}

const useExtratoContaCorrente = () => {
    const context = useContext(ExtratoContaCorrenteContext);
  
    if (!context) {
      throw new Error('useExtratoContaCorrente must be used within an ExtratoContaCorrenteProvider');
    }
  
    return context;
};

export { ExtratoContaCorrenteProvider, useExtratoContaCorrente };