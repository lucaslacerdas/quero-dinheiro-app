import React, {useState, useEffect, useRef} from 'react';
import {
  ScrollView,
  StatusBar,
  Dimensions,
  SafeAreaView,
  ImageBackground,
} from 'react-native';

import {useNavigation, useIsFocused} from '@react-navigation/native';
import BlockInfo from '../../components/BlockInfo';
import Colors from '../../config/colors';
import Button from '../../components/Button';

import fundo from '../../assets/images/bg_Inicial.png';

import {useAuth} from '../../hooks/auth';

import {useAlert} from '../../hooks/Alert';
import {Container, Header, Middle, Footer, Label, LabelValor} from './styles';
import {Card, Row} from 'native-base';

export default function ConfirmarProposta() {
  const navigation = useNavigation();
  return (
    <SafeAreaView style={{flex: 1}}>
      <StatusBar
        hidden
        barStyle="light-content"
        backgroundColor={Colors.white}
      />
      <ScrollView
        style={{
          backgroundColor: Colors.white,
        }}
        contentContainerStyle={{flexGrow: 1}}
        showsVerticalScrollIndicator={false}>
        <Container
          style={{
            backgroundColor: 'transparent',
            alignItems: 'center',
            justifyContent: 'space-around',
          }}>
           
          <Card style={{padding: 20, width:'80%', alignItems:'center', justifyContent:'center'}}>
          <Label>
            Valor Solicitado:
          </Label>
          <LabelValor>
            R$ 5000,00:
          </LabelValor>
          
            <BlockInfo
              Info="FGTS"
              Label="Banco"
            />
            

            {/* <Row style={{height:'auto'}}>
              <BlockInfo
                Info="36"
                Label="Parcelas"
                width="35%"
              />
              <BlockInfo
                Info="R$ 100,00 "
                Label="Pagamento mensal"
                width="55%"
              />
            </Row> */}
            <BlockInfo
              Info="Taxa CET a.m"
              Label="Taxa CET a.m"
            width="100%"
            />
            <BlockInfo
              Info="30,7% ao ano"
              Label="Taxa CET a.a"
            width="100%"
            />
            <BlockInfo
              Info="R$ 1.307,00"
              Label="Valor total com juros"
            width="100%"
            />
          </Card>

       
          <Button
                  backgoundColor={Colors.primary}
                  borderColor={Colors.primary}
                  onPress={() => {
                   navigation.navigate('ConfirmarPropostaTexto')
                  }}
                  textColor={Colors.white}
                  rightIconName="arrow-right"
                  style={{marginTop: 20, marginBottom: 20}}>
                  Continuar
                </Button>
         
        </Container>
      </ScrollView>
    </SafeAreaView>
  );
}
