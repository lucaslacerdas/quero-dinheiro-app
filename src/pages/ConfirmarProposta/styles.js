import styled from 'styled-components/native';
import {Platform, TouchableOpacity, Dimensions} from 'react-native';
import {StyleSheet} from 'react-native';

import Colors from '../../config/colors';

const fontOpenRegular = Platform.OS === 'ios' ? 'OpenSans' : 'OpenSans-Regular';

export const Container = styled.View`
  flex: 1;
  padding: 0;
  background-color: ${Colors.white};
`;
export const Header = styled.View`
  width:100%;
  height:30%;
  align-items:center;
  justify-content:center;
`;
export const Middle = styled.View`
  width:100%;
  height:40%;
  align-items:center;
  justify-content:center;
`;
export const Footer = styled.View`
  width:100%;
  height:30%;
  align-items:center;
  justify-content:center;
`;
export const Label = styled.Text`
  color:${Colors.grayText};
  align-self:flex-start
  font-family: ${fontOpenRegular};
  font-size: 11px;
  
`;
export const LabelValor = styled.Text`
  color:${Colors.primary};
  align-self:flex-start
  font-family: ${fontOpenRegular};
  font-size: 18px;
  font-weight:bold;
  margin-bottom:20px;
`;
export const styles = StyleSheet.create({
  ViewSaldo: {
    paddingTop: 20,
    paddingBottom: 25,
    paddingLeft: 10,
    paddingRight: 10,
  },
  ImageBackground: {
    width: '100%',
    height: 240,
  },
});
