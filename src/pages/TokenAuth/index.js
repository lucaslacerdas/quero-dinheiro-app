import React, {useState, useEffect, useRef} from 'react';
import {
  View,
  ScrollView,
  Animated,
  StatusBar,
  Text,
  Platform,
  Image,
  SafeAreaView,
  Dimensions,
  TouchableOpacity,
} from 'react-native';

import {useNavigation, useIsFocused} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import IconMaterial from 'react-native-vector-icons/MaterialIcons';
import Colors from '../../config/colors';
import Button from '../../components/Button';
import InputTextCircle from '../../components/InputTextCircle';
import logo from '../../assets/images/logo.png';
import {Form} from '@unform/mobile';
import HandleRequest from '../../services/HandleRequest';
import {CountdownCircleTimer} from 'react-native-countdown-circle-timer';
// import Beviguard from '../../components/Beviguard';
import Input from '../../components/Input';
import {useAuth} from '../../hooks/auth';
import {useAlert} from '../../hooks/Alert';
import { validarSms } from '../../services/ValidarSmsService';
import {
  styles,
  Container,
  InfoBoxBottom,
  TextInfoBoxBottom,
  InfoBoxBottomBlank,
  TextInfoBoxBottomBlank,
  Header,
  Middle,
  Footer,
  StepBox,
  SplitInput,
  Label,
  LabelHighlight,
} from './styles';

export default function TokenAuth() {
  const [key, setKey] = useState(0);
  const FirstInputRef = useRef();
  const SecondInputRef = useRef();
  const ThirdInputRef = useRef();
  const FourthInputRef = useRef();
  const FifthInputRef = useRef();
  const SixthInputRef = useRef();
  const [firstInput, setFirstInput] = useState(null);
  const [secondInput, setSecondInput] = useState(null);
  const [thirdInput, setThirdInput] = useState(null);
  const [fourthInput, setFourthInput] = useState(null);
  const [fifthInput, setFifthInput] = useState(null);
  const [sixthInput, setSixthInput] = useState(null);
  const [isPlaying, setIsPlaying] = useState(true);
  const { showAlert, hideAlert } = useAlert();
const [duration, setDuration] = useState(90);

const children = ({ remainingTime }) => {
  const minutes = Math.floor(remainingTime / 60)
  const seconds = remainingTime % 60
    console.log(remainingTime);
  return `${minutes}m ${seconds}s`
}
const formatRemainingTime = time => {
  const minutes = Math.floor((time % 3600) / 60);
  const seconds = time % 60;

  return `${minutes}m ${seconds}s`;
};
const navigation = useNavigation();
const renderTime = ({ remainingTime, animatedColor }) => {
var time = formatRemainingTime(remainingTime);
if(remainingTime==0)
{
  return (<Animated.Text style={{color: Colors.white, width:'200%', textAlign:'center'}}>
    CÓDIGO EXPIRADO
    </Animated.Text>);
  
}
  return (<Animated.Text style={{color: Colors.white}}>
    {time}
    </Animated.Text>);
  // formatRemainingTime(remainingTime);
};
const prosseguir = async ()=>{

  try { 
  var  token = "ASDWER";
    console.log("token: "+token);
    var dados = {
      token:token
    };
    console.log('dados: '+dados);

    const [handleSubmitResponse, handleSubmitError] = await HandleRequest(
      validarSms(dados)
    );
   
//  handleSubmitResponse.success = true; // apenas enquanto não houver requisição bloop 
//     if (handleSubmitError || !handleSubmitResponse.success) {
//       throw new Error(
//         'Um erro inesperado ocorreu, tente novamente mais tarde.',
//       );
//     }
            
                  navigation.navigate('Home');
  } catch(err)
  {
    showAlert({
      title: 'Atenção!',
      message: 'Um erro inesperado ocorreu ao tentar validar o token, tente novamente mais tarde.',
      titleStyle: { color: Colors.red, fontSize: 28 },
      showConfirmButton: true,
      confirmText: 'OK !',
      confirmButtonColor: Colors.primary,
      closeOnTouchOutside: false,
      closeOnHardwareBackPress: false,
      onConfirmPressed: () => hideAlert(),
  });
  
}
}
  return (
    <SafeAreaView style={{flex: 1}}>
      <StatusBar
        barStyle="light-content"
        hidden
        backgroundColor={Colors.backgroundPurple}
      />
      <ScrollView
        style={{
          backgroundColor: Colors.backgroundPurple,
          // position: Platform.OS === 'ios' ? 'absolute' : 'relative'
        }}
        contentContainerStyle={{flexGrow: 1}}
        showsVerticalScrollIndicator={false}>
        <Container
          style={{
            backgroundColor: Colors.backgroundPurple,
            alignItems: 'center',
            justifyContent: 'space-around',
          }}>
          <Header>
            <Label><LabelHighlight>Insira</LabelHighlight> o código {'\n'} de 6 dígitos que {'\n'}você recebeu via SMS</Label>
          </Header>
          <Middle>
            
            <View

              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginBottom:30,
                width:'90%'
              }}>
              <InputTextCircle
                ref={FirstInputRef}
                maxLength={1}
                isFilled
                value={firstInput}
                autoCorrect={false}
                onChangeText={e => {
                  setFirstInput(e);
                }}
                onKeyPress={({nativeEvent}) => {
                  if (nativeEvent.key !== 'Backspace') {
                    SecondInputRef.current.focus();
                  }
                }}
              />

              <InputTextCircle
                ref={SecondInputRef}
                maxLength={1}
                isFilled
                onChangeText={e => {
                  setSecondInput(e);
                }}
                onKeyPress={({nativeEvent}) => {
                  if (nativeEvent.key === 'Backspace') {
                    FirstInputRef.current.focus();
                  } else {
                    ThirdInputRef.current.focus();
                  }
                }}
              />

              <InputTextCircle
                ref={ThirdInputRef}
                maxLength={1}
                isFilled
                onChangeText={e => {
                  setThirdInput(e);
                }}
                onKeyPress={({nativeEvent}) => {
                  if (nativeEvent.key === 'Backspace') {
                    SecondInputRef.current.focus();
                  } else {
                    FourthInputRef.current.focus();
                  }
                }}
              />

              <InputTextCircle
                ref={FourthInputRef}
                maxLength={1}
                isFilled
                onChangeText={e => {
                  setFourthInput(e);
                }}
                onKeyPress={({nativeEvent}) => {
                  if (nativeEvent.key === 'Backspace') {
                    ThirdInputRef.current.focus();
                  } else {
                    FifthInputRef.current.focus();
                  }
                }}
              />

              <InputTextCircle
                ref={FifthInputRef}
                maxLength={1}
                isFilled
                onChangeText={e => {
                  if (e) {
                    setFifthInput(e);
                    SixthInputRef.current.focus();
                  } else {
                    setFifthInput('');
                    FourthInputRef.current.focus();
                  }
                }}
                onKeyPress={({nativeEvent}) => {
                  if (nativeEvent.key === 'Backspace') {
                    FifthInputRef.current.focus();
                  }
                }}
              />

              <InputTextCircle
                ref={SixthInputRef}
                maxLength={1}
                isFilled
                onChangeText={e => {
                  if (!e) {
                    setSixthInput('');
                    FifthInputRef.current.focus();
                  } else {
                    setSixthInput(e);
                    prosseguir();
                  }
                }}
              />
            </View>
{isPlaying? <Label style={{marginBottom:20}}>Código expira em</Label>: <Label style={{marginBottom:20, color:'transparent'}}>Código expira em</Label>
           }
          
            <CountdownCircleTimer
            key={key}
              isPlaying={isPlaying}
              duration={duration}
              trailColor={Colors.backgroundPurple}
          
             onComplete={() => setIsPlaying(false)}
              children={renderTime}
              size={100}
                
                trailStrokeWidth={1}
                strokeWidth={5}
               
              colors={[
                ['#494B8D', 0.5],
                ['#1DF0C7', 0.8],
                ['#1DF0C8', 0.8],
                
               
              ]}>
                  {renderTime}
            
            </CountdownCircleTimer>
          </Middle>
          <Footer>
            <TouchableOpacity style={{width:'100%',flexDirection:"row", alignItems:"center", justifyContent:"center"}} onPress={()=>{
              
              isPlaying?false:
              setKey(prevKey => prevKey + 1)
              setIsPlaying(true)
              }}>
              <IconMaterial name="refresh" style={{color:Colors.primary, fontSize:20}}/>
              <Text  style={{color:Colors.white, marginLeft:5}}>
                Reenviar código de confirmação
              </Text>
            </TouchableOpacity>
            <TouchableOpacity style={{marginTop:20,width:'100%',flexDirection:"row", alignItems:"center", justifyContent:"center"}} onPress={()=>{
              alert('To Do')
              }}>
              <IconMaterial name="help" style={{color:Colors.primary, fontSize:30}}/>
              <Text  style={{color:Colors.white, marginLeft:15}}>
                Não encontrei
              </Text>
            </TouchableOpacity>
          </Footer>
        </Container>
      </ScrollView>
    </SafeAreaView>
  );
}
