import React, {useState, useEffect, useRef} from 'react';
import {
  ScrollView,
  Animated,
  StatusBar,
  View,
  Dimensions,
  Image,
  SafeAreaView,
  TouchableOpacity,
  Text
} from 'react-native';
import {Card} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';

import IconAnt from 'react-native-vector-icons/AntDesign';

import IconMaterial from 'react-native-vector-icons/MaterialCommunityIcons';

import moment from 'moment';
import {useNavigation, useIsFocused, useRoute} from '@react-navigation/native';
import DateTimePicker from '@react-native-community/datetimepicker';
import Colors from '../../config/colors';
import Button from '../../components/Button';
import {Picker} from '@react-native-picker/picker';
import logo from '../../assets/images/logo.png';
import {Form} from '@unform/mobile';
import * as Yup from 'yup';
import HandleRequest from '../../services/HandleRequest';
import {consultaCep, salvarDados} from '../../services/ConfirmarDadosService';
import ImagePicker from 'react-native-image-crop-picker';
import Input from '../../components/Input';

import {useAuth} from '../../hooks/auth';

import {useAlert} from '../../hooks/Alert';
import {
  styles,
  Container,
  InfoBoxBottom,
  TextInfoBoxBottom,
  InfoBoxBottomBlank,
  TextInfoBoxBottomBlank,
  Header,
  Middle,
  LabelTop,
  LabelTopHighlight,
  Footer,
  Select,
  LabelInput,
  Row,
  Column,
  StepBox,
  SplitInput,
  Label,
  IconCardBox,
  LabelCard,
  LabelCardSub,
} from './styles';

import InputHollow from '../../components/InputHollow';
import {setTextRange} from 'typescript';
import { salvarImagem } from '../../services/EnviarDocumentosService';

export default function EnvioDocumentos() {
  const route = useRoute();
  const [isLoading, setIsLoading] = useState(false);
  const isPageFocused = useIsFocused();
  const [currentStep, setCurrentStep] = useState('1');
  const [imagemCNH, setImagemCNH] = useState(null);
  const [rgFrente, setRgFrente] = useState(null);
  const [rgVerso, setRgVerso] = useState(null);
  const [tipoDoc, setTipoDoc] = useState(1);
  const formRef = useRef(null);
  //
  const useCameraToTakePicture = async (doc) => {
    try {
      // setIsloading(true);
      const ret = await ImagePicker.openCamera({
       
        cropping: true,
        compressImageQuality: 1,
        includeBase64: true,
        writeTempFile: false,
        maxFiles: 1,
        cropperCancelText: 'Cancelar',
        cropperChooseText: 'Confirmar',
      });
      
     if(doc==1) //RG FRENTE
      {setRgFrente(ret.path);
      setCurrentStep(3);
      }
      if(doc==2) //RG Verso
      {setRgVerso(ret.path);
      setCurrentStep(4);
      }
      if(doc==3) //CNH
      {setImagemCNH(ret.path);
      setCurrentStep(5);
      }
      
      // if (!Array.isArray(ret)) {
      //   onEvent({
      //     data: ret.data,
      //     mime: ret.mime,
      //   });
      // }

      // setIsloading(false);
    } catch (error) {
      console.log('error', error);
      // setIsloading(false);
    }
  };

  const useDocumentPicker = async (doc) => {
    try {
      // setIsloading(true);
      const ret = await ImagePicker.openPicker({
        width: 300,
        height: 400,
        cropping: true,
        compressImageQuality: 0.1,
        includeBase64: true,
        writeTempFile: false,
        maxFiles: 1,
        cropperCancelText: 'Cancelar',
        cropperChooseText: 'Confirmar',
      });

      if (!Array.isArray(ret)) {
        onEvent({
          data: ret.data,
          mime: ret.mime,
        });
      }

      // setIsloading(false);
    } catch (error) {
      console.log('error', error);
      // setIsloading(false);
    }
  };

  const onTapButton = (opt = 1, doc=1) => {
    if (opt === 1) {
      useCameraToTakePicture(doc);
    }

    if (opt === 2) {
      useDocumentPicker(doc);
    }
  };
  const handleSubmit = async () => {
    try {
      var dados = {
        rgFrente: rgFrente,
        rgVerso: rgVerso,
        cnh: imagemCNH,
        tipo: tipoDoc,
      };

      // const [handleSubmitResponse, handleSubmitError] = await HandleRequest(
      //   salvarImagem(dados),
      // );
      // handleSubmitResponse.success = true; // apenas enquanto não houver requisição bloop
      // if (handleSubmitError || !handleSubmitResponse.success) {
      //   throw new Error(
      //     'Um erro inesperado ocorreu, tente novamente mais tarde.',
      //   );
      // }

      navigation.navigate('ConfirmarProposta');
      setCurrentStep(1);
    } catch (err) {
      showAlert({
        title: 'Atenção!',
        message:
          'Um erro inesperado ocorreu ao tentar efetuar o cadastro, tente novamente mais tarde.',
        titleStyle: {color: Colors.red, fontSize: 28},
        showConfirmButton: true,
        confirmText: 'OK !',
        confirmButtonColor: Colors.primary,
        closeOnTouchOutside: false,
        closeOnHardwareBackPress: false,
        onConfirmPressed: () => hideAlert(),
      });

      // setRefreshing(false);
      // setIsLoading(false);
    }

  };

  const navigation = useNavigation();

  return (
    <SafeAreaView style={{flex: 1}}>
      <StatusBar
        barStyle="light-content"
        hidden
        backgroundColor={Colors.backgroundPurple}
      />
      <ScrollView
        style={{
          backgroundColor: Colors.white,
        }}
        contentContainerStyle={{flexGrow: 1}}
        showsVerticalScrollIndicator={false}>
        {currentStep == 1 ? (
          <Container
            style={{
              backgroundColor: Colors.white,
              alignItems: 'center',
              justifyContent: 'flex-start',
              padding: 20,
            }}>
            <Header>
              <LabelTop>
                Para continuar, {'\n'}{' '}
                <LabelTopHighlight> escolha um documento </LabelTopHighlight>{' '}
                {'\n'}para finalizar a proposta:
              </LabelTop>
            </Header>
            <Middle>
              <TouchableOpacity
                onPress={() => {
                  onTapButton(1,3);
                  setTipoDoc(1)
                }}>
                <Card style={{width: '80%', padding: 20, paddingVertical: 35}}>
                  <Row>
                    <Column
                      style={{
                        width: '20%',
                        marginRight: 20,
                        alignItems: 'center',
                      }}>
                      <IconCardBox>
                        <Row
                          style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                          }}>
                          <IconAnt
                            name="user"
                            size={28}
                            color={Colors.primary}
                          />
                          <IconMaterial name="menu" size={28} />
                        </Row>
                      </IconCardBox>
                    </Column>
                    <Column style={{width: '80%'}}>
                      <LabelCard>CNH</LabelCard>
                      <LabelCardSub>
                        Carteira Nacional de Habilitação
                      </LabelCardSub>
                    </Column>
                  </Row>
                </Card>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  setCurrentStep(2);
                  setTipoDoc(2)
                }}>
                <Card style={{width: '80%', padding: 20, paddingVertical: 35}}>
                  <Row>
                    <Column
                      style={{
                        width: '20%',
                        marginRight: 20,
                        alignItems: 'center',
                      }}>
                      <IconCardBox style={{width: '75%'}}>
                        <IconAnt name="user" size={25} color={Colors.primary} />
                        <IconMaterial name="fingerprint" size={20} />
                      </IconCardBox>
                    </Column>
                    <Column style={{width: '80%'}}>
                      <LabelCard>RG</LabelCard>
                      <LabelCardSub>Documento de Identidade</LabelCardSub>
                    </Column>
                  </Row>
                </Card>
              </TouchableOpacity>
            </Middle>

            <Footer></Footer>
          </Container>
        ) : null}

        {currentStep == 2 ? (
          <Container
            style={{
              backgroundColor: Colors.white,
              alignItems: 'center',
              justifyContent: 'flex-start',
              padding: 20,
            }}>
            <Header>
              <IconCardBox style={{width: '12%'}}>
                <IconAnt name="user" size={28} color={Colors.primary} />
                <IconMaterial name="fingerprint" size={25} />
              </IconCardBox>
              <LabelTop>
                <LabelTopHighlight> Frente </LabelTopHighlight>
                do seu {'\n'}
                documento
              </LabelTop>
            </Header>

            <Middle >
              <LabelTop>
                Para prosseguir, tire uma foto {'\n'} da frente do seu RG.
              </LabelTop>
              <LabelCard  style={ { textAlign:"center", fontSize:16, width:"80%" } }> Assegure-se de tirar uma foto nítida na posição vertical.</LabelCard>
            </Middle>
            <Footer>
              <Button onPress= { ()=> onTapButton(1,1) } borderColor={Colors.primary} backgoundColor={Colors.primary} textColor={Colors.white}> <IconAnt size={17} style={{marginRight:20}} name="camera" color="white"></IconAnt> Fotografar</Button>
            </Footer>
          </Container>
        ) : null}
{
  currentStep == 3 ?
  <Container
  style={{
    backgroundColor: Colors.white,
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: 20,
  }}>
  <Header></Header>
  <Middle style={{marginTop:-50}}>
    <View style={{borderRadius:10, borderColor:Colors.primary, borderWidth:3,padding:5,width:'50%'}}>
      <Image source = {{ uri:rgFrente }}  style={{ borderRadius:2.5, width:'100%', height:'100%',resizeMode: 'stretch'}} />
    </View>
  <LabelCardSub style={ { fontSize:15,textAlign:'center', marginTop:20 } } >Antes de enviar, verifique {'\n'}se a foto está nítida e na vertical.</LabelCardSub> 
  </Middle>

  <Footer>
  <Button onPress= { ()=> onTapButton(1,2) } borderColor={Colors.primary} backgoundColor={Colors.primary} textColor={Colors.white}> <IconAnt size={17} style={{marginRight:20}} name="camera" color="white"></IconAnt> Fotografar verso</Button>
<TouchableOpacity onPress= { ()=> onTapButton(1,1) } style={{width:'80%'}}>
  <LabelCardSub style={ { fontSize:13,textAlign:'center', marginTop:20 } }>Tirar Novamente</LabelCardSub>
</TouchableOpacity>
    </Footer>
  </Container>

  :null
}
{
  currentStep == 4 ?
  <Container
  style={{
    backgroundColor: Colors.white,
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: 20,
  }}>
  <Header></Header>
  <Middle style={{marginTop:-50}}>
    <View style={{borderRadius:10, borderColor:Colors.primary, borderWidth:3,padding:5,width:'50%'}}>
      <Image source = {{ uri:rgVerso }}  style={{ borderRadius:2.5, width:'100%', height:'100%',resizeMode: 'stretch'}} />
    </View>
  <LabelCardSub style={ { fontSize:15,textAlign:'center', marginTop:20 } } >Antes de enviar, verifique {'\n'}se a foto está nítida e na vertical.</LabelCardSub> 
  </Middle>

  <Footer>
  <Button onPress= { ()=> handleSubmit() } borderColor={Colors.primary} backgoundColor={Colors.primary} textColor={Colors.white}>Enviar</Button>
<TouchableOpacity onPress= { ()=> onTapButton(1,1) } style={{width:'80%'}}>
  <LabelCardSub style={ { fontSize:13,textAlign:'center', marginTop:20 } }>Tirar Novamente</LabelCardSub>
</TouchableOpacity>
    </Footer>
  </Container>

  :null
}

{
  currentStep == 5 ?
  <Container
  style={{
    backgroundColor: Colors.white,
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: 20,
  }}>
  <Header></Header>
  <Middle style={{marginTop:-50}}>
    <View style={{borderRadius:10, borderColor:Colors.primary, borderWidth:3,padding:5,width:'50%'}}>
      <Image source = {{ uri:imagemCNH }}  style={{ borderRadius:2.5, width:'100%', height:'100%',resizeMode: 'stretch'}} />
    </View>
  <LabelCardSub style={ { fontSize:15,textAlign:'center', marginTop:20, width:'80%' } } >Antes de enviar, verifique {'\n'}se a foto está nítida, com o documento aberto e na vertical.</LabelCardSub> 
  </Middle>

  <Footer>
  <Button onPress= { ()=> handleSubmit() } borderColor={Colors.primary} backgoundColor={Colors.primary} textColor={Colors.white}>Enviar</Button>
<TouchableOpacity onPress= { ()=> onTapButton(1,3) } style={{width:'80%'}}>
  <LabelCardSub style={ { fontSize:13,textAlign:'center', marginTop:20 } }>Tirar Novamente</LabelCardSub>
</TouchableOpacity>
    </Footer>
  </Container>

  :null
}

      </ScrollView>
    </SafeAreaView>
  );
}
