import styled from 'styled-components/native';
import {Platform, TouchableOpacity, Dimensions} from 'react-native';
import {StyleSheet} from 'react-native';

import Colors from '../../config/colors';

const fontOpenRegular = Platform.OS === 'ios' ? 'OpenSans' : 'OpenSans-Regular';

export const Container = styled.View`
  flex: 1;
  padding: 0;

  background-color: ${Colors.white};
`;

export const ContainerAtalhos = styled.View`
  flex-direction: row;
  padding: 0 15px 0 15px;
`;

export const Select = styled.View`
color: ${Colors.grayTheme};
font-family: ${Platform.OS === 'ios' ? "OpenSans" : "OpenSans-Regular"};
width: ${parseInt(Dimensions.get('window').width * 0.75)}px;
margin-top: 15px;

padding-left: 20px;
border-color:${Colors.white};

border-radius: 40px;
background: rgba(233, 237, 243, 0.8);
elevation:1;
margin-bottom:15px;
`;


export const StepBox = styled.View`
  background-color: ${Colors.white};
  padding: 10px;
  border-radius: 5px;
  width: 87%;
  align-items: center;
  align-self: center;
`;

export const ContainerSaldo = styled.View`
  flex-direction: column;
  height: 220px;
`;
export const InfoBoxBottom = styled.View`
  padding: 10px;
  width: 80%;
  border-color: ${Colors.primary};
  border-radius: 5px;
  border-width: 1px;
`;

export const InfoBoxBottomBlank = styled.View`
  padding: 10px;
  width: 87%;
  border-color: transparent;
`;

export const Label = styled.Text`
    align-self:center;
    font-weight:bold;
`;

export const LabelInput = styled.Text`
    align-self:flex-start;
    margin-left:10px;
    font-weight:bold;
    color:${Colors.backgroundPurple}
    margin-bottom:-10px;

`;

export const TextInfoBoxBottom = styled.Text`
  color: ${Colors.white};
  font-size: 12px;
`;

export const TextInfoBoxBottomBlank = styled.Text`
  color: transparent;
  font-size: 15px;
`;

export const ContainerBeviguard = styled.View`
  margin-top: 15px;
  width: 100%;
  flex-direction: row;
  padding: 20px;
`;
export const SplitInput = styled.View`
  width:100%;
  flex-direction:row;
  align-items:center;
  justify-content:space-between;
`;
export const Header = styled.View`
  width:100%;
  height:30%;
  align-items:center;
  justify-content:center;
`;
export const Middle = styled.View`
  width:100%;
  height:40%;
  align-items:center;
  justify-content:center;
`;
export const Footer = styled.View`
  width:100%;
  height:30%;
  align-items:center;
  justify-content:center;
`;


export const ImageBackground = styled.ImageBackground``;

export const TextHome = styled.Text`
  color: ${props => props.color || Colors.white};
  font-size: ${props => props.size}px;
  font-family: ${fontOpenRegular};
`;

export const LabelTop = styled.Text`
        color: ${Colors.textPurple};
        font-weight:bold;
 
        font-size:18px;
        margin-top:30px;
        margin-bottom:20px;
        text-align:center;
        align-self:center;
`;

export const LabelTopHighlight = styled.Text`
        color: ${Colors.primary};
        font-weight:bold;
        
        font-size:18px;
        margin-top:30px;
        margin-bottom:20px;
        text-align:center;
        align-self:center;
`;
export const ViewSaldo = styled.View`
  border-bottom-width: 1px;
  border-style: solid;
  border-color: rgba(255, 255, 255, 0.2);
  padding-left: 15px;
  padding-right: 5px;
  margin-bottom: 10px;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

export const IconCardBox = styled.View`
padding-vertical:5px;

border-color:${Colors.textPurple};
border-width:5px;
border-radius:10px;
align-items:center;
justify-content:center; 

`;

export const ViewTabela = styled.View`
  width: 50%;
  padding: 10px;
  height: 100%;
`;

export const TextSaldo = styled.Text`
  color: ${Colors.white};
  font-size: 48px;
  align-items: center;
  justify-content: center;
  font-family: 'OpenSans-Bold';
`;

export const ButtonTelaSaque = styled(TouchableOpacity)`
  background-color: ${Colors.greenTheme};
  text-transform: uppercase;
  padding: 10px;
  border-radius: 8px;
  font-family: 'OpenSans-Bold';
  height: 60px;
  justify-content: center;
  align-items: center;
  flex-direction: row;
`;

export const ButtonHideSaldo = styled(TouchableOpacity)`
  color: ${Colors.white};
  padding: 10px;
`;

export const ViewCarousel = styled.View`
  width: 100%;
  padding: 10px;
  height: 100%;
`;

export const ViewTelaSaque = styled.View`
  width: 100%;
  padding: 5px;
  height: 100%;
`;
export const Row = styled.View`
flex-direction:row;


`;
export const Column = styled.View`
flex-direction:column;


`;
export const LabelCard = styled.Text`
color:${Colors.primary};
font-size:20px;
font-weight:bold;
`;
export const LabelCardSub = styled.Text`
color:${Colors.backgroundPurple};
font-size:12px;
font-weight:bold;
`;
export const styles = StyleSheet.create({
  ViewSaldo: {
    paddingTop: 20,
    paddingBottom: 25,
    paddingLeft: 10,
    paddingRight: 10,
  },
  ImageBackground: {
    width: '100%',
    height: 240,
  },
});
