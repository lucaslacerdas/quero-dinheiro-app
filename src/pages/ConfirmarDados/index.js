import React, {useState, useEffect, useRef} from 'react';
import {
  ScrollView,
  Animated,
  StatusBar,
  View,
  Image,
  SafeAreaView,
} from 'react-native';
import moment from 'moment';
import {useNavigation, useIsFocused, useRoute} from '@react-navigation/native';
import DateTimePicker from '@react-native-community/datetimepicker';
import Colors from '../../config/colors';
import Button from '../../components/Button';
import {Picker} from '@react-native-picker/picker';
import logo from '../../assets/images/logo.png';
import {Form} from '@unform/mobile';
import * as Yup from 'yup';
import HandleRequest from '../../services/HandleRequest';
import {consultaCep, salvarDados} from '../../services/ConfirmarDadosService';

import Input from '../../components/Input';

import {useAuth} from '../../hooks/auth';

import {useAlert} from '../../hooks/Alert';
import {
  styles,
  Container,
  InfoBoxBottom,
  TextInfoBoxBottom,
  InfoBoxBottomBlank,
  TextInfoBoxBottomBlank,
  Header,
  Middle,
  Footer,
  Select,
  LabelInput,
  Row,
  Column,
  StepBox,
  SplitInput,
  Label,
} from './styles';
import {LabelTop} from '../Home/styles';
import InputHollow from '../../components/InputHollow';
import {setTextRange} from 'typescript';

const cpfMask = value => {
  return value
    .replace(/\D/g, '') // substitui qualquer caracter que nao seja numero por nada
    .replace(/(\d{3})(\d)/, '$1.$2') // captura 2 grupos de numero o primeiro de 3 e o segundo de 1, apos capturar o primeiro grupo ele adiciona um ponto antes do segundo grupo de numero
    .replace(/(\d{3})(\d)/, '$1.$2')
    .replace(/(\d{3})(\d{1,2})/, '$1-$2')
    .replace(/(-\d{2})\d+?$/, '$1'); // captura 2 numeros seguidos de um traço e não deixa ser digitado mais nada
};

const dateMask = value => {
  var v = value.replace(/D/g, '');
  v = v.replace(/(d{2})(d)/, '$1/$2');
  v = v.replace(/(d{2})(d)/, '$1/$2');

  return v;
};

const telMask = value => {
  value = value.replace(/\D/g, ''); //Remove tudo o que não é dígito
  value = value.replace(/^(\d{2})(\d)/g, '($1) $2'); //Coloca parênteses em volta dos dois primeiros dígitos
  value = value.replace(/(\d)(\d{4})$/, '$1-$2');
  return value;
};

export default function ConfirmaDados() {
  const route = useRoute();

  const isPageFocused = useIsFocused();
  const formRef = useRef(null);
  //
  function handleInput(cpf) {
    var cpf_formatado = cpfMask(cpf);
    setCpf(cpf_formatado);
  }
  function handleInputData(data) {
    var data_formatada = dateMask(data);
    setDataNasc(data_formatada);
  }
  function handleInputTel(telefone) {
    var telefone_formatado = telMask(telefone);
    setTelefone(telefone_formatado);
  }

  const consultarCep = async () => {
    try {
      if (cep.length == 8) {
        const [handleSubmitResponse, handleSubmitError] = await HandleRequest(
          consultaCep(cep),
        );

        // apenas enquanto não houver requisição bloop
        // if (handleSubmitError) {
        //   throw new Error(
        //     'Um erro inesperado ocorreu ao consultar o cep, informe os dados de endereço manualmente',
        //   );
        // }
        if (handleSubmitResponse.bairro) {
          setBairro(handleSubmitResponse.bairro);
        }
        if (handleSubmitResponse.localidade) {
          setCidade(handleSubmitResponse.localidade);
        }
        if (handleSubmitResponse.uf) {
          setUf(handleSubmitResponse.uf);
        }
        if (handleSubmitResponse.logradouro) {
          setEndereco(handleSubmitResponse.logradouro);
        }
      }
      numeroRef.current.focus();
    } catch (error) {
      showAlert({
        title: 'Atenção!',
        message: error.message,
        titleStyle: {color: Colors.red, fontSize: 28},
        showConfirmButton: true,
        confirmText: 'OK !',
        confirmButtonColor: Colors.primary,
        closeOnTouchOutside: false,
        closeOnHardwareBackPress: false,
        onConfirmPressed: () => hideAlert(),
      });

      // setRefreshing(false);
      // setIsLoading(false);
    }
  };
  const handleSubmit = async () => {
    try {
      var dados = {
        cpf: cpf,
        nome: nome,
        telefone: telefone,
        senha: senha,
      };

      const [handleSubmitResponse, handleSubmitError] = await HandleRequest(
        cadastraEGeraSms(dados),
      );
      handleSubmitResponse.success = true; // apenas enquanto não houver requisição bloop
      if (handleSubmitError || !handleSubmitResponse.success) {
        throw new Error(
          'Um erro inesperado ocorreu, tente novamente mais tarde.',
        );
      }

      navigation.navigate('TokenAuth');
    } catch (err) {
      showAlert({
        title: 'Atenção!',
        message:
          'Um erro inesperado ocorreu ao tentar efetuar o cadastro, tente novamente mais tarde.',
        titleStyle: {color: Colors.red, fontSize: 28},
        showConfirmButton: true,
        confirmText: 'OK !',
        confirmButtonColor: Colors.primary,
        closeOnTouchOutside: false,
        closeOnHardwareBackPress: false,
        onConfirmPressed: () => hideAlert(),
      });

      // setRefreshing(false);
      // setIsLoading(false);
    }
  };

  async function handleStep(step) {
    console.log(step)
    if (step == 1) {
      try {
        var dados = {
          sexo: sexo,
          nascimento: dataNascDisplay,
          estadoCivil: estadoCivil,
          nomeMae: nomeMae,
          email: email,
        };

        const schema = Yup.object().shape({
          nascimento: Yup.string()
            .required('Informe a data de nascimento')
            .nullable(),
          sexo: Yup.string().required('Informe o sexo').nullable(),
          nomeMae: Yup.string().required('Informe o nome da mãe').nullable(),
          estadoCivil:Yup.string().required('Informe o estado civil').nullable(),
          email:Yup.string().required('Informe o e-mail').email('E-mail inválido').nullable()
        });
        var options = {
          abortEarly: false,
          recursive: true,
        };
        await schema.validate(dados, options);

        const [handleSubmitResponse, handleSubmitError] = await HandleRequest(
          salvarDados(dados),
        );
        fadeOut(fadeAnim);
        setCurrentStep(2);
        fadeIn(fadeAnim2);
      } catch (err) {
        var stringObj = '{ ';
        err.inner.map(erro => {
          if (stringObj == '{ ')
            stringObj =
              stringObj + ' "' + erro.path + '" :' + '"' + erro.message + '" ';
          else
            stringObj =
              stringObj + ', "' + erro.path + '":' + '"' + erro.message + '" ';

          // PERCORRER ERROS E SETAR DINAMINCAMENTE PARA CADA CAMPO, COMPARAR AS MENSAGENS PARA IDENTIFICAR QUAL ERRO É QUAL
        });
        stringObj = stringObj + ' }';
        console.log(stringObj);
        const Erros = JSON.parse(stringObj);
        if (Erros) {
          formRef.current.setErrors(Erros);
        }
      }
    }
    if (step == 2) {
      try {

        var dados = {
          rg: rg,
          emissao: dataEmissaoDisplay,
          nacionalidade: nacionalidade,
          ufNaturalidade: ufNaturalidade,
          cidadeNaturalidade: cidadeNaturalidade,
          trabalhaAtualmente:trabalhaAtualmente,
          profissao:profissao,
          admissao:dataAdmissaoDisplay,
          renda:renda
        };
        const schema = Yup.object().shape({
            rg: Yup.string()
            .required('Informe o RG')
            .min(9, 'Informe o RG completo')
            .nullable(),
        
            emissao:Yup.string()
            .required('Informe a data de emissão do RG')
            .nullable() ,
        
            nacionalidade: Yup.string()
            .required('Informe a sua nacionalidade')
            .nullable(),
        
            ufNaturalidade: Yup.string()
            .required('Informe o estado de naturalidade')
            
            .nullable(),
        
            cidadeNaturalidade: Yup.string()
            .required('Informe a cidade de naturalidade')
            .min(3, 'Informe pelo menos 3 caracteres')
            .nullable(),
        
            trabalhaAtualmente:Yup.string()
            .required('Informe se trabalha atualmente')
            .nullable(),
        
            profissao:Yup.string()
            .required('Informe a profissão')
            .min(3, 'Informe ao menos 3 caracteres')
            .nullable(),
        
            admissao:Yup.string()
            .required('Informe a data de admissão na empresa')
            .nullable(),
        
            renda:Yup.string()
            .required('Informe a sua renda')
            .nullable()  
          });

          var options = {
            abortEarly: false,
            recursive: true,
          };
          await schema.validate(dados, options);
  
        const [handleSubmitResponse, handleSubmitError] = await HandleRequest(
          salvarDados(dados),
        );
        fadeOut(fadeAnim2);
        setCurrentStep(3);
        fadeIn(fadeAnim3);
      } catch (err) {
        var stringObj = '{ ';
        err.inner.map(erro => {
          if (stringObj == '{ ')
            stringObj =
              stringObj + ' "' + erro.path + '" :' + '"' + erro.message + '" ';
          else
            stringObj =
              stringObj + ', "' + erro.path + '":' + '"' + erro.message + '" ';

          // PERCORRER ERROS E SETAR DINAMINCAMENTE PARA CADA CAMPO, COMPARAR AS MENSAGENS PARA IDENTIFICAR QUAL ERRO É QUAL
        });
        stringObj = stringObj + ' }';
        console.log(stringObj);
        const Erros = JSON.parse(stringObj);
        if (Erros) {
          formRef.current.setErrors(Erros);
        }
      }
    }
    if (step == 3) {
      console.log('caiu');
      try {
        var dados = {
          cep: cep,
          uf: uf,
          cidade: cidade,
          endereco: endereco,
          bairro: bairro,
          complemento:complemento,
          numero: numero
                  };
                  console.log(dados)
        const schema = Yup.object().shape({
          cep: Yup.string()
          .required('Informe o cep completo')
          .min(8, 'Informe o cep completo')
          .nullable(),
          uf: Yup.string()
          .required('Informe o estado ')
          .nullable(),
          cidade: Yup.string()
          .required('Informe a cidade')
          .min(3, 'Informe pelo menos 3 caracteres')
          .nullable(),
          endereco: Yup.string()
          .required('Informe o endereço')
          .min(3, 'Informe pelo menos 3 caracteres')
          .nullable(),
          bairro: Yup.string()
          .required('Informe o bairro')
          .min(3, 'Informe pelo menos 3 caracteres')
          .nullable(),
          numero: Yup.string()
          .required('Informe o número')
          .nullable()
        });
console.log(schema);
        var options = {
          abortEarly: false,
          recursive: true,
        };
        await schema.validate(dados, options);
        const [handleSubmitResponse, handleSubmitError] = await HandleRequest(
          salvarDados(dados),
        );
        fadeOut(fadeAnim3);
        setCurrentStep(4);
        fadeIn(fadeAnim4);
      } catch (err) {
        
        var stringObj = '{ ';
        err.inner.map(erro => {
          if (stringObj == '{ ')
            stringObj =
              stringObj + ' "' + erro.path + '" :' + '"' + erro.message + '" ';
          else
            stringObj =
              stringObj + ', "' + erro.path + '":' + '"' + erro.message + '" ';

          // PERCORRER ERROS E SETAR DINAMINCAMENTE PARA CADA CAMPO, COMPARAR AS MENSAGENS PARA IDENTIFICAR QUAL ERRO É QUAL
        });
        stringObj = stringObj + ' }';
        console.log(stringObj);
        const Erros = JSON.parse(stringObj);
        if (Erros) {
          formRef.current.setErrors(Erros);
        }
      }
    }
    if (step == 4) {
      try {
     

   
        var dados = {
          banco: banco,
          tipoConta: tipoConta,
          agencia: agencia,
          digitoAgencia: digitoAgencia,
          conta: conta,
          digitoConta: digitoConta,
        };

           const schema = Yup.object().shape({
            banco: Yup.string()
            .required('Informe o banco')
            .nullable(),
            tipoConta: Yup.string()
            .required('Informe o tipo da conta')
            .nullable(),
            agencia: Yup.string()
            .required('Informe a agencia e dígito')
            .nullable(),
            digitoAgencia: Yup.string()
            .required('*')
            .nullable(),
            conta: Yup.string()
            .required('Informe a conta e dígito')
            .nullable(),
            digitoConta: Yup.string()
            .required('*')
            .nullable(),
        });

        var options = {
          abortEarly: false,
          recursive: true,
        };
        await schema.validate(dados, options);
        const [handleSubmitResponse, handleSubmitError] = await HandleRequest(
          salvarDados(dados),
        );
        fadeOut(fadeAnim4);
        setCurrentStep(5);
        fadeIn(fadeAnim5);
      } catch (err) {
        var stringObj = '{ ';
        err.inner.map(erro => {
          if (stringObj == '{ ')
            stringObj =
              stringObj + ' "' + erro.path + '" :' + '"' + erro.message + '" ';
          else
            stringObj =
              stringObj + ', "' + erro.path + '":' + '"' + erro.message + '" ';

          // PERCORRER ERROS E SETAR DINAMINCAMENTE PARA CADA CAMPO, COMPARAR AS MENSAGENS PARA IDENTIFICAR QUAL ERRO É QUAL
        });
        stringObj = stringObj + ' }';
        console.log(stringObj);
        const Erros = JSON.parse(stringObj);
        if (Erros) {
          formRef.current.setErrors(Erros);
        }
      }
    }
    if (step == 5) {
      try {
      

        var dados = {
          afinidade: afinidade,
          telefone: telefone,
          nomeAfinidade: nomeAfinidade,
        };
  const schema = Yup.object().shape({
            afinidade: Yup.string()
            .required('Informe o grau de afinidade')
            .nullable(),
          telefone: Yup.string()
          .required('Informe o telefone')
          .min(14, 'Informe ao menos 10 dígitos')
          .max(15, 'Informe no máximo 11 dígitos')
          .nullable(),
          nomeAfinidade: Yup.string()
          .required('Informe o nome da referência')
          .min(3, 'Informe ao menos 3 caracteres')
          .nullable(),
        });
        var options = {
          abortEarly: false,
          recursive: true,
        };
        await schema.validate(dados, options);

        const [handleSubmitResponse, handleSubmitError] = await HandleRequest(
          salvarDados(dados),
        );
        navigation.navigate('EnvioDocumentos');
      } catch (err) {
        var stringObj = '{ ';
        err.inner.map(erro => {
          if (stringObj == '{ ')
            stringObj =
              stringObj + ' "' + erro.path + '" :' + '"' + erro.message + '" ';
          else
            stringObj =
              stringObj + ', "' + erro.path + '":' + '"' + erro.message + '" ';

          // PERCORRER ERROS E SETAR DINAMINCAMENTE PARA CADA CAMPO, COMPARAR AS MENSAGENS PARA IDENTIFICAR QUAL ERRO É QUAL
        });
        stringObj = stringObj + ' }';
        console.log(stringObj);
        const Erros = JSON.parse(stringObj);
        if (Erros) {
          formRef.current.setErrors(Erros);
        }
      }
    }
  }
  const handleBackStep = step => {
    if (step == 2) {
      fadeOut(fadeAnim2);
      setCurrentStep(1);
      fadeIn(fadeAnim);
    }
    if (step == 3) {
      fadeOut(fadeAnim3);
      setCurrentStep(2);
      fadeIn(fadeAnim2);
    }
    if (step == 4) {
      fadeOut(fadeAnim3);
      setCurrentStep(3);
      fadeIn(fadeAnim3);
    }
    if (step == 5) {
      fadeOut(fadeAnim5);
      setCurrentStep(4);
      fadeIn(fadeAnim4);
    }
  };
  const showMode = currentMode => {
    setShow(true);
    setMode(currentMode);
  };

  const showDatepicker = () => {
    showMode('date');
  };

  const showMode2 = currentMode => {
    setShow2(true);
    setMode2(currentMode);
  };
  const showMode3 = currentMode => {
    setShow3(true);
    setMode3(currentMode);
  };
  const showDatepicker2 = () => {
    showMode2('date');
  };
  const showDatepicker3 = () => {
    showMode3('date');
  };
  const onChangeDatePicker = (event, selectedDate) => {
    if (selectedDate && selectedDate != null) {
      var date = new Date();
      const currentDate = selectedDate || date;
      setShow(Platform.OS === 'ios');
      setDataNasc(currentDate);
      var nova_data_display = moment(currentDate).format('DD/MM/YYYY');
      setDataNascDisplay(nova_data_display);
    }
  };
  const onChangeDatePicker2 = (event, selectedDate) => {
    if (selectedDate && selectedDate != null) {
      var date = new Date();
      const currentDate = selectedDate || date;
      setShow2(Platform.OS === 'ios');
      setDataEmissao(currentDate);
      var nova_data_display = moment(currentDate).format('DD/MM/YYYY');
      setDataEmissaoDisplay(nova_data_display);
    }
  };
  const getMoney = str => {
    return parseInt(str.replace(/[\D]+/g, ''));
  };
  const formatReal = int => {
    var tmp = int + '';
    tmp = tmp.replace(/([0-9]{2})$/g, ',$1');
    if (tmp.length > 6) tmp = tmp.replace(/([0-9]{3}),([0-9]{2}$)/g, '.$1,$2');

    return tmp;
  };

  const trataRenda = value => {
    var valor = '';
    if (value) {
      valor = value;
      valor = valor + '';
      valor = parseInt(valor.replace(/[\D]+/g, ''));
      valor = valor + '';
      valor = valor.replace(/([0-9]{2})$/g, ',$1');
    }

    // var valor = getMoney(value);
    // valor = formatReal(valor);
    setRenda(valor);
  };
  const onChangeDatePicker3 = (event, selectedDate) => {
    if (selectedDate && selectedDate != null) {
      var date = new Date();
      const currentDate = selectedDate || date;
      setShow3(Platform.OS === 'ios');
      setDataAdmissao(currentDate);
      var nova_data_display = moment(currentDate).format('DD/MM/YYYY');

      setDataAdmissaoDisplay(nova_data_display);
    }
  };
  useEffect(() => {
    setCurrentStep(1);
  }, [currentStep]);
  const navigation = useNavigation();
  // const [currentStep, setCurrentStep] = step? useState(1) :useState(1);
  const [currentStep, setCurrentStep] = useState(1);
  const [cpf, setCpf] = useState(null);
  const cpfRef = useRef();
  const [nome, setNome] = useState(null);
  const nomeRef = useRef();
  const [sexo, setSexo] = useState('M');
  const sexoRef = useRef();
  const [nomeMae, setNomeMae] = useState('');
  const nomeMaeRef = useRef();
  const [email, setEmail] = useState('');
  const emailRef = useRef();
  const [senha, setSenha] = useState(null);
  const senhaRef = useRef();
  const [cep, setCep] = useState('');
  const cepRef = useRef();
  const [uf, setUf] = useState('');
  const ufRef = useRef();
  const [cidade, setCidade] = useState('');
  const cidadeRef = useRef();
  const [bairro, setBairro] = useState('');
  const bairroRef = useRef();
  const [endereco, setEndereco] = useState('');
  const enderecoRef = useRef();
  const [complemento, setComplemento] = useState('');
  const complementoRef = useRef();
  const [numero, setNumero] = useState('');
  const numeroRef = useRef();
  const [banco, setBanco] = useState('001');
  const bancoRef = useRef();
  const [tipoConta, setTipoConta] = useState('CC');
  const tipoContaRef = useRef();
  const [agencia, setAgencia] = useState('');
  const agenciaRef = useRef();
  const [digitoAgencia, setDigitoAgencia] = useState('');
  const digitoAgenciaRef = useRef();
  const [conta, setConta] = useState('');
  const contaRef = useRef();
  const [digitoConta, setDigitoConta] = useState('');
  const digitoContaRef = useRef();
  const [dataNasc, setDataNasc] = useState(new Date());
  const dataNascRef = useRef();
  const [dataNascDisplay, setDataNascDisplay] = useState('');
  const dataNascDisplayRef = useRef();
  const [estadoCivil, setEstadoCivil] = useState('1');
  const estadoCivilRef = useRef();
  const [show, setShow] = useState(false);
  const [trabalhaAtualmente, setTrabalhaAtualmente] = useState('1');
  const [show2, setShow2] = useState(false);
  const [show3, setShow3] = useState(false);
  const [dataAdmissao, setDataAdmissao] = useState(new Date());
  const [dataAdmissaoDisplay, setDataAdmissaoDisplay] = useState('');
  const dataAdmissaoDisplayRef = useRef();
  const [renda, setRenda] = useState('');
  const rendaRef = useRef();
  const [telefone, setTelefone] = useState('');
  const telefoneRef = useRef();

  const [tipoFone, setTipoFone] = useState('');
  const tipoFoneRef = useRef();
  const [nomeAfinidade, setNomeAfinidade] = useState('');
  const nomeAfinidadeRef = useRef();
  const [mode, setMode] = useState('date');
  const [mode2, setMode2] = useState('date');
  const [mode3, setMode3] = useState('date');

  const [rg, setRg] = useState(null);
  const rgRef = useRef();
  const [dataEmissao, setDataEmissao] = useState(new Date());
  const dataEmissaoRef = useRef();
  const [dataEmissaoDisplay, setDataEmissaoDisplay] = useState('');
  const dataEmissaoDisplayRef = useRef();
  const [profissao, setProfissao] = useState('');
  const profissaoRef = useRef();
  const [nacionalidade, setNacionalidade] = useState(null);
  const nacionalidadeRef = useRef();
  const [ufNaturalidade, setUfNaturalidade] = useState(null);
  const urNaturalidadeRef = useRef();
  const [cidadeNaturalidade, setCidadeNaturalidade] = useState(null);
  const cidadeNaturalidadeRef = useRef();
  const {showAlert, hideAlert} = useAlert();
  const fadeAnim = useRef(new Animated.Value(0)).current;
  const fadeAnim2 = useRef(new Animated.Value(0)).current;
  const fadeAnim3 = useRef(new Animated.Value(0)).current;
  const fadeAnim4 = useRef(new Animated.Value(0)).current;
  const [afinidade, setAfinidade] = useState(1);
  const fadeAnim5 = useRef(new Animated.Value(0)).current;

  const fadeIn = fade => {
    Animated.timing(fade, {
      toValue: 1,
      duration: 300,
      useNativeDriver: true,
    }).start();
  };

  const fadeOut = fade => {
    Animated.timing(fadeAnim, {
      toValue: 0,
      duration: 300,
      useNativeDriver: true,
    }).start();
  };

  fadeIn(fadeAnim);

  return (
    <SafeAreaView style={{flex: 1}}>
      <StatusBar
        barStyle="light-content"
        hidden
        backgroundColor={Colors.backgroundPurple}
      />
      <ScrollView
        style={{
          backgroundColor: Colors.white,
        }}
        contentContainerStyle={{flexGrow: 1}}
        showsVerticalScrollIndicator={false}>
        <Container
          style={{
            backgroundColor: Colors.white,
            alignItems: 'center',
            justifyContent: 'flex-start',
          }}>
          <Form ref={formRef}>
            {/* Step 1 */}
            {currentStep == 1 ? (
              <Animated.View style={{opacity: fadeAnim, width: '100%'}}>
                <LabelTop>Dados Pessoais</LabelTop>

                {/* <InputHollow
                  refInput={nomeRef}
                  onSubmitEditing={() => cpfRef.current.focus()}
                  label="Nome"
                  name="nome"
                  value={nome}
                  onChangeText={value => setNome(value)}
                /> */}

                {/* <InputHollow
                  label="CPF"
                  keyboardType="numeric"
                  refInput={cpfRef}
                  onSubmitEditing={() => dataNascDisplayRef.current.focus()}
                  name="cpf"
                  value={cpf}
                  onChangeText={value => handleInput(value)}
                /> */}

                <LabelInput>Sexo</LabelInput>
                <Select>
                  <Picker
                    selectedValue={sexo}
                    onValueChange={value => setSexo(value)}>
                    <Picker.Item value="M" label="Masculino" />
                    <Picker.Item value="F" label="Feminino" />
                  </Picker>
                </Select>
                <Select>
                  {show && (
                    <DateTimePicker
                      testID="dataNasc"
                      value={dataNasc}
                      mode={mode}
                      display="calendar"
                      onChange={onChangeDatePicker}
                    />
                  )}
                </Select>
                <InputHollow
                  label="Data de nascimento"
                  name="nascimento"
                  refInput={dataNascDisplayRef}
                  onSubmitEditing={() => nomeMaeRef.current.focus()}
                  value={dataNascDisplay}
                  onFocus={() => {
                    showDatepicker();
                  }}
                  readonly
                />
                <LabelInput>Estado Civil</LabelInput>
                <Select>
                  <Picker
                    selectedValue={estadoCivil}
                    onValueChange={value => setEstadoCivil(value)}>
                    <Picker.Item value="1" label="Solteiro" />
                    <Picker.Item value="2" label="Casado" />
                    <Picker.Item value="3" label="Viúvo" />
                    <Picker.Item value="4" label="Separado Judicialmente" />
                    <Picker.Item value="5" label="Divorciado" />
                  </Picker>
                </Select>

                <InputHollow
                  label="Nome da Mãe"
                  refInput={nomeMaeRef}
                  onSubmitEditing={() => emailRef.current.focus()}
                  name="nomeMae"
                  value={nomeMae}
                  onChangeText={value => setNomeMae(value)}
                />

                <InputHollow
                  label="Email"
                  name="email"
                  value={email}
                  refInput={emailRef}
                  onSubmitEditing={() => handleStep(1)}
                  onChangeText={value => setEmail(value)}
                />
                <Button
                  backgoundColor={Colors.primary}
                  borderColor={Colors.primary}
                  onPress={() => {
                    handleStep(1);
                  }}
                  textColor={Colors.white}
                  rightIconName="arrow-right"
                  style={{marginTop: 20, marginBottom: 20}}>
                  Continuar
                </Button>
              </Animated.View>
            ) : null}

            {/* Step 2 */}
            {currentStep == 2 ? (
              <Animated.View style={{opacity: fadeAnim, width: '100%'}}>
                <LabelTop>Dados Pessoais</LabelTop>

                <InputHollow
                  label="RG"
                  name="rg"
                  maxLength={9}
                  value={rg}
                  refInput={rgRef}
                  onSubmitEditing={() => dataEmissaoDisplayRef.current.focus()}
                  onChangeText={value => setRg(value)}
                />

                {show2 && (
                  <DateTimePicker
                    testID="dataEmissao"
                    value={dataEmissao}
                    mode={mode2}
                    display="calendar"
                    onChange={onChangeDatePicker2}
                  />
                )}
                <InputHollow
                  label="Data de Emissão"
                  name="emissao"
                  value={dataEmissaoDisplay}
                  refInput={dataEmissaoDisplayRef}
                  onSubmitEditing={() => nacionalidadeRef.current.focus()}
                  onFocus={showDatepicker2}
                />

                <InputHollow
                  label="Nacionalidade"
                  refInput={nacionalidadeRef}
                  onSubmitEditing={() => cidadeNaturalidadeRef.current.focus()}
                  name="nacionalidade"
                  value={nacionalidade}
                  onChangeText={value => setNacionalidade(value)}
                />

                <LabelInput>UF de Naturalidade</LabelInput>
                <Select>
                  <Picker
                    selectedValue={ufNaturalidade}
                    onValueChange={value => setUfNaturalidade(value)}>
                    <Picker.Item label="Acre" value="AC" />
                    <Picker.Item label="Alagoas" value="AL" />
                    <Picker.Item label="Amapá" value="AP" />
                    <Picker.Item label="Amazonas" value="AM" />
                    <Picker.Item label="Bahia" value="BA" />
                    <Picker.Item label="Ceará" value="CE" />
                    <Picker.Item label="Espírito Santo" value="ES" />
                    <Picker.Item label="Goiás" value="GO" />
                    <Picker.Item label="Maranhão" value="MA" />
                    <Picker.Item label="Mato Grosso" value="MT" />
                    <Picker.Item label="Mato Grosso do Sul" value="MS" />
                    <Picker.Item label="Minas Gerais" value="MG" />
                    <Picker.Item label="Pará" value="PA" />
                    <Picker.Item label="Paraíba" value="PB" />
                    <Picker.Item label="Paraná" value="PR" />
                    <Picker.Item label="Pernambuco" value="PE" />
                    <Picker.Item label="Piauí" value="PI" />
                    <Picker.Item label="Rio de Janeiro" value="RJ" />
                    <Picker.Item label="Rio Grande do Norte" value="RN" />
                    <Picker.Item label="Rio Grande do Sul" value="RS" />
                    <Picker.Item label="Rondônia" value="RO" />
                    <Picker.Item label="Roraima" value="RR" />
                    <Picker.Item label="Santa Catarina" value="SC" />
                    <Picker.Item label="São Paulo" value="SP" />
                    <Picker.Item label="Sergipe" value="SE" />
                    <Picker.Item label="Tocantins" value="TO" />
                    <Picker.Item label="Distrito Federal" value="DF" />
                  </Picker>
                </Select>

                <InputHollow
                  label="Cidade de naturalidade"
                  name="cidadeNaturalidade"
                  refInput={cidadeNaturalidadeRef}
                  onSubmitEditing={() => handleStep(2)}
                  value={cidadeNaturalidade}
                  onChangeText={value => setCidadeNaturalidade(value)}
                />

                <LabelInput>Trabalha atualmente ?</LabelInput>
                <Select>
                  <Picker
                    selectedValue={trabalhaAtualmente}
                    onValueChange={value => setTrabalhaAtualmente(value)}>
                    <Picker.Item label="SIM" value="2" />
                    <Picker.Item label="NÃO" value="1" />
                  </Picker>
                </Select>

                {trabalhaAtualmente == 2 ? (
                  <>
                    <InputHollow
                      label="Profissão"
                      name="profissao"
                      refInput={profissaoRef}
                      onSubmitEditing={() => null}
                      value={profissao}
                      onChangeText={value => setProfissao(value)}
                    />
                    {show3 && (
                      <DateTimePicker
                        testID="dataAdmissao"
                        value={dataAdmissao}
                        mode={mode3}
                        display="calendar"
                        onChange={onChangeDatePicker3}
                      />
                    )}
                    <InputHollow
                      label="Data de Admissão"
                      name="admissao"
                      value={dataAdmissaoDisplay}
                      refInput={dataAdmissaoDisplayRef}
                      onSubmitEditing={() => rendaRef.current.focus()}
                      onFocus={showDatepicker3}
                    />
                    <InputHollow
                      label="Renda atual"
                      name="renda"
                      refInput={rendaRef}
                      onSubmitEditing={() => null}
                      value={renda}
                      keyboardType="numeric"
                      onChangeText={value => trataRenda(value)}
                    />

                    {/* <LabelInput>Profissão</LabelInput> */}
                    {/* <Select>
                      <Picker
                        selectedValue={profissao}
                        onValueChange={value => setProfissao(value)}>
                        <Picker.Item value="340" label="ANALISTA" />
                        <Picker.Item value="1210" label="LABORATORISTA" />
                        <Picker.Item value="121110" label="LIDER" />
                        <Picker.Item value="1145" label="CUIDADOR DE IDOSOS" />
                        <Picker.Item value="1218" label="VIGILANTE" />
                        <Picker.Item value="399" label="CENOGRAFIA" />
                        <Picker.Item value="400" label="OPERADOR" />
                        <Picker.Item value="1619" label="ESTOQUISTA" />
                        <Picker.Item value="376" label="BOMBEIRO" />
                        <Picker.Item value="1624" label="CONFERENTE" />
                        <Picker.Item value="121157" label="ATENDENTE" />
                        <Picker.Item value="1143" label="CONCIERGE" />
                        <Picker.Item value="3" label="ANALISTAS DE SISTEMAS" />
                        <Picker.Item
                          value="69"
                          label="FUNCIONARIO PUBLICO (NIVEL TECNICO)"
                        />
                        <Picker.Item value="102" label="ARQUITETO (OUTROS)" />
                        <Picker.Item value="107" label="ARQUITETO" />
                        <Picker.Item value="108" label="AUXILIAR/ASSISTENTE" />
                        <Picker.Item value="109" label="ANALISTA" />
                        <Picker.Item value="110" label="ASCENSORISTA" />
                        <Picker.Item value="111" label="ATLETA PROFISSIONAL" />
                        <Picker.Item value="112" label="ATOR" />
                        <Picker.Item value="113" label="AUDITOR/INSPETOR" />
                        <Picker.Item value="114" label="BALCONISTA" />
                        <Picker.Item value="115" label="BANCARIO" />
                        <Picker.Item value="116" label="BARMAN" />
                        <Picker.Item value="117" label="BIBLIOTECARIO" />
                        <Picker.Item value="119" label="CABELEIREIRO" />
                        <Picker.Item value="120" label="CONFEITEIRO" />
                        <Picker.Item value="121" label="CONSULTOR" />
                        <Picker.Item value="122" label="CONTADOR" />
                        <Picker.Item value="123" label="CONTROLER" />
                        <Picker.Item value="124" label="COSTUREIRA" />
                        <Picker.Item value="125" label="COZINHEIRO" />
                        <Picker.Item value="126" label="COORDENADOR" />
                        <Picker.Item value="127" label="DENTISTA" />
                        <Picker.Item value="128" label="DESENHISTA" />
                        <Picker.Item
                          value="129"
                          label="DIRETOR SUPERINTENDENTE"
                        />
                        <Picker.Item value="130" label="ECONOMISTA" />
                        <Picker.Item
                          value="131"
                          label="EMPREGADO DOMESTICO (BABA, FAXINEIRA, GOVERNANTA)"
                        />
                        <Picker.Item value="132" label="ENFERMEIRO" />
                        <Picker.Item value="133" label="ENGENHEIRO" />
                        <Picker.Item value="134" label="FARMACEUTICO" />
                        <Picker.Item value="135" label="FERRAMENTEIRO" />
                        <Picker.Item value="136" label="FISICO" />
                        <Picker.Item value="137" label="FISIOTERAPEUTA" />
                        <Picker.Item value="138" label="FRENTISTA" />
                        <Picker.Item value="139" label="FRESADOR" />
                        <Picker.Item value="140" label="FUNILEIRO" />
                        <Picker.Item value="141" label="GARCOM" />
                        <Picker.Item value="142" label="GERENTE" />
                        <Picker.Item value="143" label="INSTRUMENTISTA" />
                        <Picker.Item value="144" label="INSTRUTOR" />
                        <Picker.Item
                          value="145"
                          label="JORNALISTA/REPORTER/REDATOR/EDITOR"
                        />
                        <Picker.Item value="146" label="LOCUTOR/RADIALISTA" />
                        <Picker.Item value="147" label="MECANICO" />
                        <Picker.Item value="148" label="MANICURE" />
                        <Picker.Item value="149" label="MANOBRISTA" />
                        <Picker.Item value="150" label="MASSAGISTA" />
                        <Picker.Item value="151" label="MEDICO" />
                        <Picker.Item value="152" label="MONITOR" />
                        <Picker.Item value="153" label="MONTADOR" />
                        <Picker.Item value="154" label="MOTOBOY" />
                        <Picker.Item value="155" label="MOTORISTA" />
                        <Picker.Item value="156" label="OPERADOR" />
                        <Picker.Item value="157" label="ADMINISTRADOR" />
                        <Picker.Item value="158" label="PADEIRO" />
                        <Picker.Item value="159" label="PASTOR" />
                        <Picker.Item value="160" label="PILOTO" />
                        <Picker.Item value="162" label="PORTEIRO" />
                        <Picker.Item value="163" label="PRESIDENTE" />
                        <Picker.Item value="164" label="PROFESSOR" />
                        <Picker.Item value="165" label="PROGRAMADOR" />
                        <Picker.Item value="166" label="PROJETISTA" />
                        <Picker.Item value="167" label="QUIMICO" />
                        <Picker.Item value="168" label="AEROMOÇA/COMISSARIO" />
                        <Picker.Item value="169" label="RECEPCIONISTA" />
                        <Picker.Item value="171" label="SECURITARIO" />
                        <Picker.Item value="172" label="SEGURANÇA" />
                        <Picker.Item value="173" label="SOLDADOR" />
                        <Picker.Item value="174" label="TECNICO" />
                        <Picker.Item value="175" label="TELEFONISTA" />
                        <Picker.Item value="176" label="TESOUREIRO" />
                        <Picker.Item value="177" label="TORNEIRO" />
                        <Picker.Item value="178" label="VENDEDOR" />
                        <Picker.Item
                          value="179"
                          label="COMANDANTE DE AERONAVES/EMBARCAÇOES"
                        />
                        <Picker.Item value="181" label="ZELADOR" />
                        <Picker.Item value="182" label="ADVOGADO" />
                        <Picker.Item value="1303" label="CONTROLADOR" />
                        <Picker.Item value="302" label="AGENTE" />
                        <Picker.Item value="304" label="ALMOXARIFE" />
                        <Picker.Item value="305" label="ARQUIVISTA" />
                        <Picker.Item value="306" label="DIRETOR" />
                        <Picker.Item value="307" label="EDITOR" />
                        <Picker.Item value="308" label="FORRADOR" />
                        <Picker.Item value="310" label="CAMAREIRO" />
                        <Picker.Item value="311" label="CONTRA REGRA" />
                        <Picker.Item value="312" label="MAQUINISTA" />
                        <Picker.Item value="313" label="CONTINUISTA" />
                        <Picker.Item value="314" label="FIGURINISTA" />
                        <Picker.Item value="315" label="REPORTER" />
                        <Picker.Item value="316" label="SONOPLASTA" />
                        <Picker.Item value="317" label="SUPERVISOR" />
                        <Picker.Item value="318" label="PRODUTOR" />
                        <Picker.Item value="319" label="ASSESSOR" />
                        <Picker.Item
                          value="320"
                          label="AUXILIAR / ASSISTENTE (OUTROS)"
                        />
                        <Picker.Item value="321" label="COMPRADOR" />
                        <Picker.Item value="322" label="ESPECIALISTA" />
                        <Picker.Item value="323" label="GARAGISTA" />
                        <Picker.Item value="324" label="MENSAGEIRO" />
                        <Picker.Item value="325" label="PARALEGAL" />
                        <Picker.Item value="326" label="PROJECT MANAGER" />
                        <Picker.Item value="327" label="REVISOR" />
                        <Picker.Item value="328" label="AUDITOR" />
                        <Picker.Item value="329" label="SUPERVISOR" />
                        <Picker.Item value="330" label="TELEFONISTA (OUTROS)" />
                        <Picker.Item value="331" label="TRADUTOR" />
                        <Picker.Item value="332" label="TREINNE" />
                        <Picker.Item value="341" label="APRENDIZ" />
                        <Picker.Item value="342" label="APRESENTADOR" />
                        <Picker.Item value="343" label="ASSISTENTE" />
                        <Picker.Item value="344" label="ATLETA" />
                        <Picker.Item value="345" label="AUXILIAR" />
                        <Picker.Item value="346" label="CANTOR" />
                        <Picker.Item value="347" label="COMPRADOR" />
                        <Picker.Item value="349" label="CONTATO" />
                        <Picker.Item value="350" label="CONTINUO" />
                        <Picker.Item value="351" label="DEMONSTRADORA" />
                        <Picker.Item value="352" label="ENCARREGADO" />
                        <Picker.Item value="353" label="ESPECIALISTA RH I" />
                        <Picker.Item value="354" label="ESTAGIARIO" />
                        <Picker.Item value="355" label="EXECUTIVO" />
                        <Picker.Item value="356" label="FOTOGRAFO" />
                        <Picker.Item value="357" label="GUARDA-ROUPEIRO" />
                        <Picker.Item value="358" label="ILUMINADOR" />
                        <Picker.Item value="359" label="ILUSTRADOR" />
                        <Picker.Item value="360" label="INSPETOR" />
                        <Picker.Item value="361" label="INSTALADOR" />
                        <Picker.Item value="362" label="LOCUTOR" />
                        <Picker.Item value="363" label="MAESTRO" />
                        <Picker.Item value="364" label="MAQUILADOR" />
                        <Picker.Item value="366" label="MUSICO" />
                        <Picker.Item value="367" label="PARCEIRO SP" />
                        <Picker.Item value="368" label="PESQUISADOR" />
                        <Picker.Item value="369" label="REDATOR" />
                        <Picker.Item value="370" label="SUPERINTENDENTE" />
                        <Picker.Item value="374" label="FUNC PUBLICO" />
                        <Picker.Item value="121767" label="DUCHISTA IV" />
                        <Picker.Item value="121972" label="SERRALHEIRO" />
                        <Picker.Item
                          value="121603"
                          label="PROSSIONAL DA ARTE,CULTURA E COMUNICACAO"
                        />
                        <Picker.Item value="121584" label="ORIENTADOR(A)" />
                        <Picker.Item value="121740" label="SALVA VIDAS" />
                        <Picker.Item value="122497" label="PROMOTOR" />
                        <Picker.Item value="122008" label="REBATEDOR " />
                        <Picker.Item value="121971" label="GESSEIRO" />
                        <Picker.Item value="122115" label="ELETRCISTA" />
                        <Picker.Item value="121970" label="MARCENEIRO" />
                        <Picker.Item value="380" label="PINTOR" />
                        <Picker.Item value="161" label="PIZZAIOLO" />
                        <Picker.Item value="170" label="SECRETARIA" />
                        <Picker.Item value="1148" label="TRANSCRITOR" />
                        <Picker.Item value="180" label="VIGIA" />
                      </Picker>
                    </Select> */}
                  </>
                ) : null}
                <Button
                  backgoundColor={Colors.primary}
                  borderColor={Colors.primary}
                  onPress={() => {
                    handleStep(2);
                  }}
                  textColor={Colors.white}
                  rightIconName="arrow-right"
                  style={{marginTop: 20, marginBottom: 20}}>
                  Continuar
                </Button>
                <Button
                  backgoundColor={Colors.purple}
                  borderColor={Colors.purple}
                  onPress={() => {
                    handleBackStep(2);
                  }}
                  textColor={Colors.white}
                  leftIconName="arrow-left"
                  style={{marginTop: 20, marginBottom: 20}}>
                  Voltar
                </Button>
              </Animated.View>
            ) : null}

            {/* Step 3 */}
            {currentStep == 3 ? (
              <Animated.View style={{opacity: fadeAnim, width: '100%'}}>
                <LabelTop>Endereço</LabelTop>

                <InputHollow
                  label="CEP"
                  name="cep"
                  refInput={cepRef}
                  onSubmitEditing={() => cidadeRef.current.focus()}
                  value={cep}
                  maxLength={8}
                  minLength={8}
                  keyboardType="numeric"
                  onChangeText={value => setCep(value)}
                  onBlur={consultarCep}
                  onSubmitEditing={consultarCep}
                />
                <LabelInput>UF</LabelInput>
                <Select>
                  <Picker
                    selectedValue={uf}
                    onValueChange={value => setUf(value)}>
                    <Picker.Item label="Acre" value="AC" />
                    <Picker.Item label="Alagoas" value="AL" />
                    <Picker.Item label="Amapá" value="AP" />
                    <Picker.Item label="Amazonas" value="AM" />
                    <Picker.Item label="Bahia" value="BA" />
                    <Picker.Item label="Ceará" value="CE" />
                    <Picker.Item label="Espírito Santo" value="ES" />
                    <Picker.Item label="Goiás" value="GO" />
                    <Picker.Item label="Maranhão" value="MA" />
                    <Picker.Item label="Mato Grosso" value="MT" />
                    <Picker.Item label="Mato Grosso do Sul" value="MS" />
                    <Picker.Item label="Minas Gerais" value="MG" />
                    <Picker.Item label="Pará" value="PA" />
                    <Picker.Item label="Paraíba" value="PB" />
                    <Picker.Item label="Paraná" value="PR" />
                    <Picker.Item label="Pernambuco" value="PE" />
                    <Picker.Item label="Piauí" value="PI" />
                    <Picker.Item label="Rio de Janeiro" value="RJ" />
                    <Picker.Item label="Rio Grande do Norte" value="RN" />
                    <Picker.Item label="Rio Grande do Sul" value="RS" />
                    <Picker.Item label="Rondônia" value="RO" />
                    <Picker.Item label="Roraima" value="RR" />
                    <Picker.Item label="Santa Catarina" value="SC" />
                    <Picker.Item label="São Paulo" value="SP" />
                    <Picker.Item label="Sergipe" value="SE" />
                    <Picker.Item label="Tocantins" value="TO" />
                    <Picker.Item label="Distrito Federal" value="DF" />
                  </Picker>
                </Select>

                <InputHollow
                  label="Cidade"
                  refInput={cidadeRef}
                  onSubmitEditing={() => enderecoRef.current.focus()}
                  value={cidade}
                  onChangeText={value => setCidade(value)}
                  name="cidade"
                />

                <InputHollow
                  label="Endereço"
                  refInput={enderecoRef}
                  onSubmitEditing={() => complementoRef.current.focus()}
                  name="endereco"
                  value={endereco}
                  onChangeText={value => setEndereco(value)}
                />

                <InputHollow
                  refInput={complementoRef}
                  onSubmitEditing={() => bairroRef.current.focus()}
                  label="Complemento"
                  name="complemento"
                  value={complemento}
                  onChangeText={value => setComplemento(value)}
                />

                <InputHollow
                  label="Bairro"
                  refInput={bairroRef}
                  onSubmitEditing={() => numeroRef.current.focus()}
                  name="bairro"
                  value={bairro}
                  onChangeText={value => setBairro(value)}
                />
                <InputHollow
                  label="Número"
                  refInput={numeroRef}
                  onSubmitEditing={() => handleStep(3)}
                  name="numero"
                  value={numero}
                  onChangeText={value => setNumero(value)}
                />

                <Button
                  backgoundColor={Colors.primary}
                  borderColor={Colors.primary}
                  onPress={() => {
                    handleStep(3);
                  }}
                  textColor={Colors.white}
                  rightIconName="arrow-right"
                  style={{marginTop: 20, marginBottom: 20}}>
                  Continuar
                </Button>
                <Button
                  backgoundColor={Colors.purple}
                  borderColor={Colors.purple}
                  onPress={() => {
                    handleBackStep(3);
                  }}
                  textColor={Colors.white}
                  leftIconName="arrow-left"
                  style={{marginTop: 20, marginBottom: 20}}>
                  Voltar
                </Button>
              </Animated.View>
            ) : null}

            {currentStep == 4 ? (
              <Animated.View style={{opacity: fadeAnim, width: '100%'}}>
                <LabelTop>Dados Bancários</LabelTop>

                <LabelInput>Banco</LabelInput>
                <Select>
                  <Picker
                    selectedValue={banco}
                    onValueChange={value => setBanco(value)}>
                    <Picker.Item value="001" label="BANCO DO BRASIL S.A (BB)	" />
                    <Picker.Item value="237" label="BRADESCO S.A	" />
                    <Picker.Item value="335" label="Banco Digio S.A	" />
                    <Picker.Item
                      value="260"
                      label="NU PAGAMENTOS S.A (NUBANK)	"
                    />
                    <Picker.Item
                      value="290"
                      label="Pagseguro Internet S.A (PagBank)	"
                    />
                    <Picker.Item value="380" label="PicPay Servicos S.A.	" />
                    <Picker.Item
                      value="323"
                      label="Mercado Pago - conta do Mercado Livre	"
                    />
                    <Picker.Item
                      value="237"
                      label="NEXT BANK (UTILIZAR O MESMO CÓDIGO DO BRADESCO)	"
                    />
                    <Picker.Item
                      value="637"
                      label="BANCO SOFISA S.A (SOFISA DIRETO)	"
                    />
                    <Picker.Item value="077" label="BANCO INTER S.A	" />
                    <Picker.Item value="341" label="ITAÚ UNIBANCO S.A	" />
                    <Picker.Item
                      value="104"
                      label="CAIXA ECONÔMICA FEDERAL (CEF)	"
                    />
                    <Picker.Item
                      value="033"
                      label="BANCO SANTANDER BRASIL S.A	"
                    />
                    <Picker.Item value="212" label="BANCO ORIGINAL S.A	" />
                    <Picker.Item
                      value="756"
                      label="BANCOOB (BANCO COOPERATIVO DO BRASIL)	"
                    />
                    <Picker.Item value="655" label="BANCO VOTORANTIM S.A	" />
                    <Picker.Item
                      value="655"
                      label="NEON PAGAMENTOS S.A (OS MESMOS DADOS DO BANCO VOTORANTIM)	"
                    />
                    <Picker.Item
                      value="041"
                      label="BANRISUL – BANCO DO ESTADO DO RIO GRANDE DO SUL S.A	"
                    />
                    <Picker.Item
                      value="389"
                      label="BANCO MERCANTIL DO BRASIL S.A	"
                    />
                    <Picker.Item value="422" label="BANCO SAFRA S.A	" />
                    <Picker.Item value="070" label="BANCO DE BRASÍLIA (BRB)	" />
                    <Picker.Item value="136" label="UNICRED COOPERATIVA	" />
                    <Picker.Item value="741" label="BANCO RIBEIRÃO PRETO	" />
                    <Picker.Item value="739" label="BANCO CETELEM S.A	" />
                    <Picker.Item value="743" label="BANCO SEMEAR S.A	" />
                    <Picker.Item
                      value="100"
                      label="PLANNER CORRETORA DE VALORES S.A	"
                    />
                    <Picker.Item value="096" label="BANCO B3 S.A	" />
                    <Picker.Item
                      value="747"
                      label="Banco RABOBANK INTERNACIONAL DO BRASIL S.A	"
                    />
                    <Picker.Item value="748" label="SICREDI S.A	" />
                    <Picker.Item value="752" label="BNP PARIBAS BRASIL S.A	" />
                    <Picker.Item value="091" label="UNICRED CENTRAL RS	" />
                    <Picker.Item value="399" label="KIRTON BANK	" />
                    <Picker.Item value="108" label="PORTOCRED S.A	" />
                    <Picker.Item
                      value="757"
                      label="BANCO KEB HANA DO BRASIL S.A	"
                    />
                    <Picker.Item value="102" label="XP INVESTIMENTOS S.A	" />
                    <Picker.Item value="348" label="BANCO XP S/A	" />
                    <Picker.Item
                      value="340"
                      label="SUPER PAGAMENTOS S/A (SUPERDITAL)	"
                    />
                    <Picker.Item
                      value="364"
                      label="GERENCIANET PAGAMENTOS DO BRASIL	"
                    />
                    <Picker.Item value="084" label="UNIPRIME NORTE DO PARANÁ	" />
                    <Picker.Item
                      value="180"
                      label="CM CAPITAL MARKETS CCTVM LTDA	"
                    />
                    <Picker.Item value="066" label="BANCO MORGAN STANLEY S.A	" />
                    <Picker.Item value="015" label="UBS BRASIL CCTVM S.A	" />
                    <Picker.Item value="143" label="TREVISO CC S.A	" />
                    <Picker.Item value="062" label="HIPERCARD BM S.A	" />
                    <Picker.Item value="074" label="BCO. J.SAFRA S.A	" />
                    <Picker.Item
                      value="099"
                      label="UNIPRIME CENTRAL CCC LTDA	"
                    />
                    <Picker.Item value="025" label="BANCO ALFA S.A.	" />
                    <Picker.Item value="075" label="BCO ABN AMRO S.A	" />
                    <Picker.Item value="040" label="BANCO CARGILL S.A	" />
                    <Picker.Item value="190" label="SERVICOOP	" />
                    <Picker.Item value="063" label="BANCO BRADESCARD	" />
                    <Picker.Item value="191" label="NOVA FUTURA CTVM LTDA	" />
                    <Picker.Item
                      value="064"
                      label="GOLDMAN SACHS DO BRASIL BM S.A	"
                    />
                    <Picker.Item
                      value="097"
                      label="CCC NOROESTE BRASILEIRO LTDA	"
                    />
                    <Picker.Item value="016" label="CCM DESP TRÂNS SC E RS	" />
                    <Picker.Item value="012" label="BANCO INBURSA	" />
                    <Picker.Item value="003" label="BANCO DA AMAZONIA S.A	" />
                    <Picker.Item value="060" label="CONFIDENCE CC S.A	" />
                    <Picker.Item
                      value="037"
                      label="BANCO DO ESTADO DO PARÁ S.A	"
                    />
                    <Picker.Item value="159" label="CASA CREDITO S.A	" />
                    <Picker.Item value="172" label="ALBATROSS CCV S.A	" />
                    <Picker.Item value="085" label="COOP CENTRAL AILOS	" />
                    <Picker.Item
                      value="114"
                      label="CENTRAL COOPERATIVA DE CRÉDITO NO ESTADO DO ESPÍRITO SANTO	"
                    />
                    <Picker.Item value="036" label="BANCO BBI S.A	" />
                    <Picker.Item
                      value="394"
                      label="BANCO BRADESCO FINANCIAMENTOS S.A	"
                    />
                    <Picker.Item
                      value="004"
                      label="BANCO DO NORDESTE DO BRASIL S.A.	"
                    />
                    <Picker.Item value="320" label="BANCO CCB BRASIL S.A	" />
                    <Picker.Item value="189" label="HS FINANCEIRA	" />
                    <Picker.Item value="105" label="LECCA CFI S.A	" />
                    <Picker.Item value="076" label="BANCO KDB BRASIL S.A.	" />
                    <Picker.Item value="082" label="BANCO TOPÁZIO S.A	" />
                    <Picker.Item value="286" label="CCR DE OURO	" />
                    <Picker.Item value="093" label="PÓLOCRED SCMEPP LTDA	" />
                    <Picker.Item
                      value="273"
                      label="CCR DE SÃO MIGUEL DO OESTE	"
                    />
                    <Picker.Item value="157" label="ICAP DO BRASIL CTVM LTDA	" />
                    <Picker.Item value="183" label="SOCRED S.A	" />
                    <Picker.Item value="014" label="NATIXIS BRASIL S.A	" />
                    <Picker.Item value="130" label="CARUANA SCFI	" />
                    <Picker.Item value="127" label="CODEPE CVC S.A	" />
                    <Picker.Item
                      value="079"
                      label="BANCO ORIGINAL DO AGRONEGÓCIO S.A	"
                    />
                    <Picker.Item
                      value="081"
                      label="BBN BANCO BRASILEIRO DE NEGOCIOS S.A	"
                    />
                    <Picker.Item
                      value="118"
                      label="STANDARD CHARTERED BI S.A	"
                    />
                    <Picker.Item value="133" label="CRESOL CONFEDERAÇÃO	" />
                    <Picker.Item value="121" label="BANCO AGIBANK S.A	" />
                    <Picker.Item
                      value="083"
                      label="BANCO DA CHINA BRASIL S.A	"
                    />
                    <Picker.Item value="138" label="GET MONEY CC LTDA	" />
                    <Picker.Item value="024" label="BCO BANDEPE S.A	" />
                    <Picker.Item
                      value="095"
                      label="BANCO CONFIDENCE DE CÂMBIO S.A	"
                    />
                    <Picker.Item value="094" label="BANCO FINAXIS	" />
                    <Picker.Item value="276" label="SENFF S.A	" />
                    <Picker.Item value="137" label="MULTIMONEY CC LTDA	" />
                    <Picker.Item value="092" label="BRK S.A	" />
                    <Picker.Item
                      value="047"
                      label="BANCO BCO DO ESTADO DE SERGIPE S.A	"
                    />
                    <Picker.Item
                      value="144"
                      label="BEXS BANCO DE CAMBIO S.A.	"
                    />
                    <Picker.Item value="126" label="BR PARTNERS BI	" />
                    <Picker.Item
                      value="301"
                      label="BPP INSTITUIÇÃO DE PAGAMENTOS S.A	"
                    />
                    <Picker.Item value="173" label="BRL TRUST DTVM SA	" />
                    <Picker.Item value="119" label="BANCO WESTERN UNION	" />
                    <Picker.Item value="254" label="PARANA BANCO S.A	" />
                    <Picker.Item value="268" label="BARIGUI CH	" />
                    <Picker.Item value="107" label="BANCO BOCOM BBM S.A	" />
                    <Picker.Item value="412" label="BANCO CAPITAL S.A	" />
                    <Picker.Item
                      value="124"
                      label="BANCO WOORI BANK DO BRASIL S.A	"
                    />
                    <Picker.Item value="149" label="FACTA S.A. CFI	" />
                    <Picker.Item value="197" label="STONE PAGAMENTOS S.A	" />
                    <Picker.Item value="142" label="BROKER BRASIL CC LTDA	" />
                    <Picker.Item
                      value="389"
                      label="BANCO MERCANTIL DO BRASIL S.A.	"
                    />
                    <Picker.Item value="184" label="BANCO ITAÚ BBA S.A	" />
                    <Picker.Item
                      value="634"
                      label="BANCO TRIANGULO S.A (BANCO TRIÂNGULO)	"
                    />
                    <Picker.Item value="545" label="SENSO CCVM S.A	" />
                    <Picker.Item value="132" label="ICBC DO BRASIL BM S.A	" />
                    <Picker.Item value="298" label="VIPS CC LTDA	" />
                    <Picker.Item value="129" label="UBS BRASIL BI S.A	" />
                    <Picker.Item
                      value="128"
                      label="MS BANK S.A BANCO DE CÂMBIO	"
                    />
                    <Picker.Item value="194" label="PARMETAL DTVM LTDA	" />
                    <Picker.Item value="310" label="VORTX DTVM LTDA	" />
                    <Picker.Item
                      value="163"
                      label="COMMERZBANK BRASIL S.A BANCO MÚLTIPLO	"
                    />
                    <Picker.Item value="280" label="AVISTA S.A	" />
                    <Picker.Item value="146" label="GUITTA CC LTDA	" />
                    <Picker.Item
                      value="279"
                      label="CCR DE PRIMAVERA DO LESTE	"
                    />
                    <Picker.Item value="182" label="DACASA FINANCEIRA S/A	" />
                    <Picker.Item
                      value="278"
                      label="GENIAL INVESTIMENTOS CVM S.A	"
                    />
                    <Picker.Item value="271" label="IB CCTVM LTDA	" />
                    <Picker.Item value="021" label="BANCO BANESTES S.A	" />
                    <Picker.Item value="246" label="BANCO ABC BRASIL S.A	" />
                    <Picker.Item value="751" label="SCOTIABANK BRASIL	" />
                    <Picker.Item value="208" label="BANCO BTG PACTUAL S.A	" />
                    <Picker.Item value="746" label="BANCO MODAL S.A	" />
                    <Picker.Item value="241" label="BANCO CLASSICO S.A	" />
                    <Picker.Item value="612" label="BANCO GUANABARA S.A	" />
                    <Picker.Item
                      value="604"
                      label="BANCO INDUSTRIAL DO BRASIL S.A	"
                    />
                    <Picker.Item
                      value="505"
                      label="BANCO CREDIT SUISSE (BRL) S.A	"
                    />
                    <Picker.Item value="196" label="BANCO FAIR CC S.A	" />
                    <Picker.Item
                      value="300"
                      label="BANCO LA NACION ARGENTINA	"
                    />
                    <Picker.Item value="477" label="CITIBANK N.A	" />
                    <Picker.Item value="266" label="BANCO CEDULA S.A	" />
                    <Picker.Item value="122" label="BANCO BRADESCO BERJ S.A	" />
                    <Picker.Item value="376" label="BANCO J.P. MORGAN S.A	" />
                    <Picker.Item
                      value="473"
                      label="BANCO CAIXA GERAL BRASIL S.A	"
                    />
                    <Picker.Item value="745" label="BANCO CITIBANK S.A	" />
                    <Picker.Item value="120" label="BANCO RODOBENS S.A	" />
                    <Picker.Item value="265" label="BANCO FATOR S.A	" />
                    <Picker.Item
                      value="007"
                      label="BNDES (Banco Nacional do Desenvolvimento Social)"
                    />
                    <Picker.Item value="188" label="ATIVA S.A INVESTIMENTOS" />
                    <Picker.Item value="134" label="BGC LIQUIDEZ DTVM LTDA" />
                    <Picker.Item value="641" label="BANCO ALVORADA S.A	" />
                    <Picker.Item
                      value="029"
                      label="BANCO ITAÚ CONSIGNADO S.A	"
                    />
                    <Picker.Item value="243" label="BANCO MÁXIMA S.A	" />
                    <Picker.Item value="078" label="HAITONG BI DO BRASIL S.A" />
                    <Picker.Item
                      value="111"
                      label="BANCO OLIVEIRA TRUST DTVM S.A	"
                    />
                    <Picker.Item value="017" label="BNY MELLON BANCO S.A	" />
                    <Picker.Item value="174" label="PERNAMBUCANAS FINANC S.A	" />
                    <Picker.Item
                      value="495"
                      label="LA PROVINCIA BUENOS AIRES BANCO	"
                    />
                    <Picker.Item value="125" label="BRASIL PLURAL S.A BANCO	" />
                    <Picker.Item value="488" label="JPMORGAN CHASE BANK	" />
                    <Picker.Item value="065" label="BANCO ANDBANK S.A	" />
                    <Picker.Item value="492" label="ING BANK N.V	" />
                    <Picker.Item value="250" label="BANCO BCV	" />
                    <Picker.Item value="145" label="LEVYCAM CCV LTDA	" />
                    <Picker.Item
                      value="494"
                      label="BANCO REP ORIENTAL URUGUAY	"
                    />
                    <Picker.Item value="253" label="BEXS CC S.A	" />
                    <Picker.Item
                      value="269"
                      label="HSBC BANCO DE INVESTIMENTO	"
                    />
                    <Picker.Item value="213" label="BCO ARBI S.A	" />
                    <Picker.Item
                      value="139"
                      label="INTESA SANPAOLO BRASIL S.A	"
                    />
                    <Picker.Item value="018" label="BANCO TRICURY S.A	" />
                    <Picker.Item value="630" label="BANCO INTERCAP S.A	" />
                    <Picker.Item value="224" label="BANCO FIBRA S.A	" />
                    <Picker.Item
                      value="600"
                      label="BANCO LUSO BRASILEIRO S.A	"
                    />
                    <Picker.Item value="623" label="BANCO PAN	" />
                    <Picker.Item
                      value="204"
                      label="BANCO BRADESCO CARTOES S.A	"
                    />
                    <Picker.Item value="479" label="BANCO ITAUBANK S.A	" />
                    <Picker.Item value="456" label="BANCO MUFG BRASIL S.A	" />
                    <Picker.Item
                      value="464"
                      label="BANCO SUMITOMO MITSUI BRASIL S.A	"
                    />
                    <Picker.Item value="613" label="OMNI BANCO S.A	" />
                    <Picker.Item
                      value="652"
                      label="ITAÚ UNIBANCO HOLDING BM S.A	"
                    />
                    <Picker.Item value="653" label="BANCO INDUSVAL S.A	" />
                    <Picker.Item value="069" label="BANCO CREFISA S.A	" />
                    <Picker.Item value="370" label="BANCO MIZUHO S.A	" />
                    <Picker.Item
                      value="249"
                      label="BANCO INVESTCRED UNIBANCO S.A	"
                    />
                    <Picker.Item value="318" label="BANCO BMG S.A	" />
                    <Picker.Item value="626" label="BANCO FICSA S.A	" />
                    <Picker.Item value="270" label="SAGITUR CC LTDA	" />
                    <Picker.Item
                      value="366"
                      label="BANCO SOCIETE GENERALE BRASIL	"
                    />
                    <Picker.Item value="113" label="MAGLIANO S.A	" />
                    <Picker.Item
                      value="131"
                      label="TULLETT PREBON BRASIL CVC LTDA	"
                    />
                    <Picker.Item
                      value="011"
                      label="C.SUISSE HEDGING-GRIFFO CV S.A (Credit Suisse)	"
                    />
                    <Picker.Item value="611" label="BANCO PAULISTA	" />
                    <Picker.Item
                      value="755"
                      label="BOFA MERRILL LYNCH BM S.A	"
                    />
                    <Picker.Item value="089" label="CCR REG MOGIANA	" />
                    <Picker.Item value="643" label="BANCO PINE S.A	" />
                    <Picker.Item
                      value="140"
                      label="EASYNVEST - TÍTULO CV S.A	"
                    />
                    <Picker.Item value="707" label="BANCO DAYCOVAL S.A	" />
                    <Picker.Item value="288" label="CAROL DTVM LTDA	" />
                    <Picker.Item value="101" label="RENASCENCA DTVM LTDA	" />
                    <Picker.Item
                      value="487"
                      label="DEUTSCHE BANK S.A BANCO ALEMÃO	"
                    />
                    <Picker.Item value="233" label="BANCO CIFRA	" />
                    <Picker.Item value="177" label="GUIDE	" />
                    <Picker.Item value="633" label="BANCO RENDIMENTO S.A	" />
                    <Picker.Item value="218" label="BANCO BS2 S.A	" />
                    <Picker.Item
                      value="292"
                      label="BS2 DISTRIBUIDORA DE TÍTULOS E INVESTIMENTOS	"
                    />
                    <Picker.Item
                      value="169"
                      label="BANCO OLÉ BONSUCESSO CONSIGNADO S.A	"
                    />
                    <Picker.Item value="293" label="LASTRO RDV DTVM LTDA	" />
                    <Picker.Item value="285" label="FRENTE CC LTDA	" />
                    <Picker.Item value="080" label="B&T CC LTDA	" />
                    <Picker.Item
                      value="753"
                      label="NOVO BANCO CONTINENTAL S.A BM	"
                    />
                    <Picker.Item
                      value="222"
                      label="BANCO CRÉDIT AGRICOLE BR S.A	"
                    />
                    <Picker.Item value="754" label="BANCO SISTEMA	" />
                    <Picker.Item value="098" label="CREDIALIANÇA CCR	" />
                    <Picker.Item value="610" label="BANCO VR S.A	" />
                    <Picker.Item value="712" label="BANCO OURINVEST S.A	" />
                    <Picker.Item value="010" label="CREDICOAMO	" />
                    <Picker.Item
                      value="283"
                      label="RB CAPITAL INVESTIMENTOS DTVM LTDA	"
                    />
                    <Picker.Item value="217" label="BANCO JOHN DEERE S.A	" />
                    <Picker.Item value="117" label="ADVANCED CC LTDA	" />
                    <Picker.Item value="336" label="BANCO C6 S.A - C6 BANK	" />
                    <Picker.Item value="654" label="BANCO DIGIMAIS S.A" />
                  </Picker>
                </Select>

                <LabelInput>Tipo de Conta</LabelInput>
                <Select>
                  <Picker
                    selectedValue={tipoConta}
                    onValueChange={value => setTipoConta(value)}>
                    <Picker.Item label="Conta Corrente" value="CC" />
                    <Picker.Item label="Conta Poupança" value="CP" />
                  </Picker>
                </Select>
                <Row>
                  <Column style={{width: '50%'}}>
                    <InputHollow
                      label="Agencia/Dígito"
                      refInput={agenciaRef}
                      onSubmitEditing={() => digitoAgenciaRef.current.focus()}
                      name="agencia"
                      value={agencia}
                      style={{width: '100%'}}
                      maxLength={9}
                      onChangeText={value => setAgencia(value)}
                      keyboardType="numeric"
                    />
                  </Column>
                  <Column style={{width: '20%', marginLeft: 15}}>
                    <InputHollow
                      refInput={digitoAgenciaRef}
                      onSubmitEditing={() => contaRef.current.focus()}
                      maxLength={2}
                      label=" "
                      name="digitoAgencia"
                      value={digitoAgencia}
                      keyboardType="numeric"
                      style={{width: '100%'}}
                      onChangeText={value => setDigitoAgencia(value)}
                    />
                  </Column>
                </Row>

                <Row>
                  <Column style={{width: '50%'}}>
                    <InputHollow
                      label="Número da Conta/Dígito"
                      name="conta"
                      refInput={contaRef}
                      onSubmitEditing={() => digitoContaRef.current.focus()}
                      style={{width: '100%'}}
                      value={conta}
                      maxLength={9}
                      keyboardType="numeric"
                      onChangeText={value => setConta(value)}
                    />
                  </Column>

                  <Column style={{width: '20%', marginLeft: 15}}>
                    <InputHollow
                      label=" "
                      refInput={digitoContaRef}
                      onSubmitEditing={() => handleStep(4)}
                      name="digitoConta"
                      maxLength={2}
                      style={{width: '100%'}}
                      value={digitoConta}
                      keyboardType="numeric"
                      onChangeText={value => setDigitoConta(value)}
                    />
                  </Column>
                </Row>

                <Button
                  backgoundColor={Colors.primary}
                  borderColor={Colors.primary}
                  onPress={() => {
                    handleStep(4);
                  }}
                  textColor={Colors.white}
                  rightIconName="arrow-right"
                  style={{marginTop: 20, marginBottom: 20}}>
                  Continuar
                </Button>
                <Button
                  backgoundColor={Colors.purple}
                  borderColor={Colors.purple}
                  onPress={() => {
                    handleBackStep(4);
                  }}
                  textColor={Colors.white}
                  leftIconName="arrow-left"
                  style={{marginTop: 20, marginBottom: 20}}>
                  Voltar
                </Button>
              </Animated.View>
            ) : null}

            {currentStep == 5 ? (
              <Animated.View style={{opacity: fadeAnim5, width: '100%'}}>
                <LabelTop>Referência Pessoal</LabelTop>

                <LabelInput>Grau de Afinidade</LabelInput>
                <Select>
                  <Picker
                    selectedValue={afinidade}
                    onValueChange={value => setAfinidade(value)}>
                    <Picker.Item label="AMIGOS(AS)" value="4">
                      AMIGOS(AS)
                    </Picker.Item>
                    <Picker.Item label="CLIENTE" value="9"></Picker.Item>
                    <Picker.Item label="COMERCIAL" value="10"></Picker.Item>
                    <Picker.Item label="ESPOSO(A)" value="7"></Picker.Item>
                    <Picker.Item label="FILHO(A)" value="8"></Picker.Item>
                    <Picker.Item label="IRMAOS(AS)" value="3"></Picker.Item>
                    <Picker.Item label="MAE" value="2"></Picker.Item>
                    <Picker.Item label="PAI" value="1"></Picker.Item>
                    <Picker.Item label="SOGRO(A)" value="6"></Picker.Item>
                    <Picker.Item label="TIOS(AS)" value="5"></Picker.Item>
                  </Picker>
                </Select>
                <InputHollow
                  label="Telefone"
                  refInput={telefoneRef}
                  onSubmitEditing={() => nomeAfinidadeRef.current.focus()}
                  name="telefone"
                  value={telefone}
                  keyboardType="numeric"
                  onChangeText={value => handleInputTel(value)}
                />
                <InputHollow
                  label="Nome *"
                  name="nomeAfinidade"
                  refInput={nomeAfinidadeRef}
                  onSubmitEditing={() => handleStep(5)}
                  value={nomeAfinidade}
                  onChangeText={value => setNomeAfinidade(value)}
                />

                <Button
                  backgoundColor={Colors.primary}
                  borderColor={Colors.primary}
                  onPress={() => {
                    handleStep(5);
                  }}
                  textColor={Colors.white}
                  rightIconName="arrow-right"
                  style={{marginTop: 20, marginBottom: 20}}>
                  Finalizar
                </Button>
                <Button
                  backgoundColor={Colors.purple}
                  borderColor={Colors.purple}
                  onPress={() => {
                    handleBackStep(5);
                  }}
                  textColor={Colors.white}
                  leftIconName="arrow-left"
                  style={{marginTop: 20, marginBottom: 20}}>
                  Voltar
                </Button>
              </Animated.View>
            ) : null}
          </Form>

          <Footer></Footer>
        </Container>
      </ScrollView>
    </SafeAreaView>
  );
}
