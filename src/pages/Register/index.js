import React, {useState, useEffect, useRef} from 'react';
import {
  ScrollView,
  Animated,
  StatusBar,
  View,
  Image,
  SafeAreaView,
} from 'react-native';

import {useNavigation, useIsFocused} from '@react-navigation/native';

import Colors from '../../config/colors';
import Button from '../../components/Button';
import logo from '../../assets/images/logo.png';
import {Form} from '@unform/mobile';
import * as Yup from 'yup';
import HandleRequest from '../../services/HandleRequest';
import {cadastraEGeraSms} from '../../services/RegisterService';

import Input from '../../components/Input';

import {useAuth} from '../../hooks/auth';

import { useAlert } from '../../hooks/Alert';
import {
  styles,
  Container,
  InfoBoxBottom,
  TextInfoBoxBottom,
  InfoBoxBottomBlank,
  TextInfoBoxBottomBlank,
  Header,
  Middle,
  Footer,
  StepBox,
  SplitInput,
  Label,
} from './styles';

 const cpfMask = value => {
  return value
    .replace(/\D/g, '') // substitui qualquer caracter que nao seja numero por nada
    .replace(/(\d{3})(\d)/, '$1.$2') // captura 2 grupos de numero o primeiro de 3 e o segundo de 1, apos capturar o primeiro grupo ele adiciona um ponto antes do segundo grupo de numero
    .replace(/(\d{3})(\d)/, '$1.$2')
    .replace(/(\d{3})(\d{1,2})/, '$1-$2')
    .replace(/(-\d{2})\d+?$/, '$1'); // captura 2 numeros seguidos de um traço e não deixa ser digitado mais nada
};

const telMask = value =>{
  value=value.replace(/\D/g,"");             //Remove tudo o que não é dígito
  value=value.replace(/^(\d{2})(\d)/g,"($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
  value=value.replace(/(\d)(\d{4})$/,"$1-$2"); 
return value
}
export default function Register() {
  const isPageFocused = useIsFocused();
  const formRef = useRef(null);
  //
  function handleInput(cpf) {
    var cpf_formatado = cpfMask(cpf);
    setCpf(cpf_formatado);
  }
  function handleInputTel(telefone) {
    var telefone_formatado = telMask(telefone);
    setTelefone(telefone_formatado);
  }
  const handleSubmit = async () => {
  
    try { 
      var dados = {
        cpf: cpf,
        nome: nome,
        telefone: telefone,
        senha: senha,
      };
    
  //     const [handleSubmitResponse, handleSubmitError] = await HandleRequest(
  //       cadastraEGeraSms(dados)
  //     );
  //  handleSubmitResponse.success = true; // apenas enquanto não houver requisição bloop 
  //     if (handleSubmitError || !handleSubmitResponse.success) {
  //       throw new Error(
  //         'Um erro inesperado ocorreu, tente novamente mais tarde.',
  //       );
  //     }
              
                    navigation.navigate('TokenAuth');
    } catch(err)
    {
      showAlert({
        title: 'Atenção!',
        message: 'Um erro inesperado ocorreu ao tentar efetuar o cadastro, tente novamente mais tarde.',
        titleStyle: { color: Colors.red, fontSize: 28 },
        showConfirmButton: true,
        confirmText: 'OK !',
        confirmButtonColor: Colors.primary,
        closeOnTouchOutside: false,
        closeOnHardwareBackPress: false,
        onConfirmPressed: () => hideAlert(),
    });
    
    // setRefreshing(false);
    // setIsLoading(false);
    }
   
  };

  async function handleStep(step) {
    if (step == 1) {
      try {
        const schema = Yup.object().shape({
          cpf: Yup.string().required('Informe o CPF').min(14,'Informe todos os dígitos do CPF').nullable(),
        });

        await schema.validate({cpf: cpf});

        fadeOut(fadeAnim);
        setCurrentStep(2);
        fadeIn(fadeAnim2);
      } catch (err) {
        if (err instanceof Yup.ValidationError) {
          const Erros = {cpf: err.message};
          formRef.current.setErrors(Erros);
        }
      }
    }
    if (step == 2) {
      try {
        const schema = Yup.object().shape({
          nome: Yup.string()
            .required('Informe o nome')
            .min(3, 'Informe ao menos 3 caracteres')
            .nullable(),
        });

        await schema.validate({nome: nome});
        fadeOut(fadeAnim2);
        setCurrentStep(3);
        fadeIn(fadeAnim3);
      } catch (err) {
        if (err instanceof Yup.ValidationError) {
          const Erros = {nome: err.message};
          formRef.current.setErrors(Erros);
        }
      }
    }
    if (step == 3) {
      try {
        const schema = Yup.object().shape({
          senha: Yup.string().required('Informe a senha').nullable(),
        });

        await schema.validate({senha: senha});
        try {
          const schema2 = Yup.object().shape({
            senhaConfirm: Yup.string()
              .required('Informe a confirmação da senha')
              .nullable(),
          });

          await schema2.validate({senhaConfirm: confirmaSenha});
          if (senha === confirmaSenha) {
            fadeOut(fadeAnim3);
            setCurrentStep(4);
            fadeIn(fadeAnim4);
          } else {
            formRef.current.setFieldError(
              'senhaConfirm',
              'As senhas devem ser idênticas',
            );
          }
        } catch (err) {
          if (err instanceof Yup.ValidationError) {
            const Erros = {};

            Erros[err.path] = err.message;

            formRef.current.setErrors(Erros);
          }
        }
      } catch (err) {
        if (err instanceof Yup.ValidationError) {
          const Erros = {};

          Erros[err.path] = err.message;

          formRef.current.setErrors(Erros);
        }
      }
    }
    if (step == 4) {

      try {
       
        const schema = Yup.object().shape({
          celular: Yup.string()
            .required('Informe o celular')
            .min(14, 'Informe ao menos 10 dígitos').max(15,'Informe no máximo 11 dígitos')
            .nullable(), 
        });
      
        await schema.validate({celular: telefone});

        handleSubmit();
      } catch (err) {
        if (err instanceof Yup.ValidationError) {
          const Erros = {};
          Erros['celular'] = err.message;
          formRef.current.setErrors(Erros);
        }
      }
   
    }
  }

  const navigation = useNavigation();
  const [currentStep, setCurrentStep] = useState(1);
  const [cpf, setCpf] = useState(null);
  const [nome, setNome] = useState(null);
  const [senha, setSenha] = useState(null);
  const [confirmaSenha, setConfirmaSenha] = useState(null);
  const [ddd, setDdd] = useState(null);
  const [telefone, setTelefone] = useState(null);
  const { showAlert, hideAlert } = useAlert();
  const fadeAnim = useRef(new Animated.Value(0)).current;
  const fadeAnim2 = useRef(new Animated.Value(0)).current;
  const fadeAnim3 = useRef(new Animated.Value(0)).current;
  const fadeAnim4 = useRef(new Animated.Value(0)).current;

  const fadeIn = fade => {
    Animated.timing(fade, {
      toValue: 1,
      duration: 300,
      useNativeDriver: true,
    }).start();
  };

  const fadeOut = fade => {
    Animated.timing(fadeAnim, {
      toValue: 0,
      duration: 300,
      useNativeDriver: true,
    }).start();
  };

  fadeIn(fadeAnim);

  return (
    <SafeAreaView style={{flex: 1}}>
      <StatusBar
        barStyle="light-content"
        backgroundColor={Colors.backgroundPurple}
      />
      <ScrollView
        style={{
          backgroundColor: Colors.backgroundPurple,
        }}
        contentContainerStyle={{flexGrow: 1}}
        showsVerticalScrollIndicator={false}>
        <Container
          style={{
            backgroundColor: Colors.backgroundPurple,
            alignItems: 'center',
            justifyContent: 'space-around',
          }}>
          <Header>
            <Image source={logo} />
          </Header>
          <Middle>
            <Form ref={formRef}>
              {/* Step 1 */}
              {currentStep == 1 ? (
                <Animated.View style={{opacity: fadeAnim, width: '100%'}}>
                  <StepBox>
                    <Input
                      name="cpf"
                      placeholder="XXX.XXX.XXX-XX"
                      label="Informe o CPF"
                      value={cpf}
                      onChangeText={value => {
                        handleInput(value);
                      }}
                      keyboardType="number-pad"
                    />

                    <Button
                      backgoundColor={Colors.primary}
                      borderColor={Colors.primary}
                      onPress={() => {
                        handleStep(1);
                      }}
                      textColor={Colors.white}
                      rightIconName="arrow-right"
                      style={{marginTop: 20}}>
                      Continuar
                    </Button>
                  </StepBox>
                </Animated.View>
              ) : null}

              {/* Step 2 */}
              {currentStep == 2 ? (
                <Animated.View style={{opacity: fadeAnim2, width: '100%'}}>
                  <StepBox>
                    <Input
                      name="nome"
                      valoue={nome}
                      label="Informe o seu nome"
                      onChangeText={value => setNome(value)}
                    />

                    <Button
                      backgoundColor={Colors.primary}
                      borderColor={Colors.primary}
                      onPress={() => {
                        handleStep(2);
                      }}
                      textColor={Colors.white}
                      rightIconName="arrow-right"
                      style={{marginTop: 20}}>
                      Continuar
                    </Button>
                  </StepBox>
                </Animated.View>
              ) : null}

              {/* Step 3 */}
              {currentStep == 3 ? (
                <Animated.View style={{opacity: fadeAnim3, width: '100%'}}>
                  <StepBox>
                    <Input
                      name="senha"
                      label="Informe a sua senha"
                      secureTextEntry
                      value={senha}
                      onChangeText={value => setSenha(value)}
                    />
                    <View style={{marginTop: 20}}></View>
                    <Input
                      name="senhaConfirm"
                      label="Confirme a sua senha"
                      secureTextEntry
                      value={confirmaSenha}
                      onChangeText={value => setConfirmaSenha(value)}
                    />
                    <Button
                      backgoundColor={Colors.primary}
                      borderColor={Colors.primary}
                      onPress={() => {
                        handleStep(3);
                      }}
                      textColor={Colors.white}
                      rightIconName="arrow-right"
                      style={{marginTop: 20}}>
                      Continuar
                    </Button>
                  </StepBox>
                </Animated.View>
              ) : null}

              {currentStep == 4 ? (
                <Animated.View style={{opacity: fadeAnim4, width: '100%'}}>
                  <StepBox>
                    <Label>Informe o celular</Label>
                      <Input
                        name="celular"
                        placeholder="(XX) XXXXX-XXXX"
                        keyboardType="phone-pad"
                        Label="Informe o celular"
                        maxLength={15}
                        value={telefone}
                        onChangeText={value => handleInputTel(value)}
                      />
                   

                    <Button
                      backgoundColor={Colors.primary}
                      borderColor={Colors.primary}
                      onPress={() => {
                        handleStep(4);
                      }}
                      textColor={Colors.white}
                      rightIconName="arrow-right"
                      style={{marginTop: 20}}>
                      Continuar
                    </Button>
                  </StepBox>
                </Animated.View>
              ) : null}
            </Form>
          </Middle>
          <Footer>
            {currentStep == 1 ? (
              <InfoBoxBottom>
                <TextInfoBoxBottom>
                  Os dados a seguir são necessários para cadastrar seu usuário,
                  sendo parte do conjunto de identificação pessoal que será
                  atribuído a você e servirá para sua identificação na
                  plataforma bevi.
                </TextInfoBoxBottom>
              </InfoBoxBottom>
            ) : (
              <InfoBoxBottomBlank>
                <TextInfoBoxBottomBlank>
                  Os dados a seguir são necessários para cadastrar seu usuário,
                  sendo parte do conjunto de identificação pessoal que será
                  atribuído a você e servirá para sua identificação na
                  plataforma bevi.
                </TextInfoBoxBottomBlank>
              </InfoBoxBottomBlank>
            )}
          </Footer>
        </Container>
      </ScrollView>
    </SafeAreaView>
  );
}
