import styled from 'styled-components/native';
import {Platform, TouchableOpacity, Dimensions} from 'react-native';
import {StyleSheet} from 'react-native';

import Colors from '../../config/colors';

const fontOpenRegular = Platform.OS === "ios" ? "OpenSans" : "OpenSans-Regular";

export const Container = styled.View`
    flex: 1;
    padding: 0;
    align-items:center;
    justify-content:center;
    background-color: ${Colors.white};
`;

export const ContainerAtalhos = styled.View`
    flex-direction: row;
    padding: 0 15px 0 15px;
`;

export const ContainerSaldo = styled.View`
    flex-direction: column;
    height: 220px;
`;
export const IconCircle = styled.View`
    height:20px;
    width:20px;
    align-items:center;
    justify-content:center;
    border-color:${Colors.primary};
    border-width:3px;
    border-radius:50px;
    margin-right:10px;
    margin-left:20px;
    margin-top:30px;
    margin-bottom:20px;

`;
export const LabelTop = styled.Text`
        color: ${Colors.textPurple};
        font-weight:bold;
        font-size:15px;
      
      
`;
export const RowLabel = styled.View`
flex-direction: row;
width: 100%;
align-items: center;
justify-content: flex-start;
margin-top:20px;
`;

export const ContainerBeviguard = styled.View`
    margin-top: 15px;
    width: 100%;
    flex-direction: row;
    padding: 20px;
`;

export const ImageBackground = styled.ImageBackground``;

export const TextHome = styled.Text`
    color: ${props => props.color || Colors.white}; 
    font-size: ${props => props.size}px;
    font-family: ${fontOpenRegular};
`;

export const ViewSaldo = styled.View`
    border-bottom-width: 1px;
    border-style: solid;
    border-color: rgba(255,255,255,.20);
    padding-left: 15px;
    padding-right: 5px;
    margin-bottom: 10px;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
`;

export const ViewTabela = styled.View`
    width: 50%; 
    padding: 10px;
    height: 100%;
`;

export const TextSaldo = styled.Text`
    color: ${Colors.white}; 
    font-size: 48px;
    align-items: center;
    justify-content: center;
    font-family: "OpenSans-Bold";
`;

export const ButtonTelaSaque = styled(TouchableOpacity)`
    background-color: ${Colors.greenTheme};
    text-transform: uppercase;
    padding: 10px;
    border-radius: 8px;
    font-family: "OpenSans-Bold";
    height: 60px; 
    justify-content: center;
    align-items: center;
    flex-direction: row;
`;

export const ButtonHideSaldo = styled(TouchableOpacity)`
    color: ${Colors.white};
    padding: 10px;
`;

export const ViewCarousel = styled.View`
    width: 100%;
    padding: 10px; 
    height: 100%; 
`;

export const ViewTelaSaque = styled.View`
    width: 100%; 
    padding: 5px;
    height: 100%;
`;

export const styles = StyleSheet.create({
    ViewSaldo: {
        paddingTop: 20, 
        paddingBottom: 25, 
        paddingLeft: 10, 
        paddingRight: 10,
    },
    ImageBackground: {
        width: '100%', 
        height: 240,
    },
});