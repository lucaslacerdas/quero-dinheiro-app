import React, {useState, useEffect, useRef} from 'react';
import {
  View,
  ScrollView,
  Animated,
  StatusBar,
  Text,
  Platform,
  RefreshControl,
  SafeAreaView,
} from 'react-native';
// import { LinearTextGradient } from "react-native-text-gradient";
// import { TextMask } from 'react-native-masked-text';
import {useNavigation, useIsFocused} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import AntIcon from 'react-native-vector-icons/AntDesign';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';

import Colors from '../../config/colors';
// import Button from '../../components/Button';
import SquareCard from '../../components/SquareCard';

// import Topo from '../../assets/bgHomeTopo.png';
// import Beviguard from '../../components/Beviguard';
// import CarouselApp from '../../components/CarouselApp';
// import IconSaque from '../../assets/ico-branco.svg';

// import Apresentacao from '../Apresentacao';

// import { useSaldoContaCorrente } from '../../hooks/saldoContaCorrente';
import {useAuth} from '../../hooks/auth';

import {styles, Container, RowLabel, LabelTop, IconCircle} from './styles';

export default MeusDados = () => {
  const isPageFocused = useIsFocused();
  //
  const {saldoContaCorrente, isSaldoLoading, getSaldo, setIsSaldoLoading} =
    useState(false);
  const {shouldViewComponent} = useAuth();
  const navigation = useNavigation();

  const [widthProps, setWidthProps] = useState();
  const [toggleSaldo, setToggleSaldo] = useState(true);
  const [shouldShowContaCorrent, setShouldShowContaCorrente] = useState(false);

  // const colorSaldoContaCorrente = (parseFloat(saldoContaCorrente) < 0) ? Colors.saldoColorNegative : Colors.white;
  // const colorSaldoContaCorrenteDegrade = (parseFloat(saldoContaCorrente) < 0) ? Colors.saldoColorNegativeDegrade : Colors.whiteDegrade;

  const fontOpenRegular =
    Platform.OS === 'ios' ? 'OpenSans' : 'OpenSans-Regular';
  const fadeAnim = useRef(new Animated.Value(0)).current;

  // const hideSaldo = () => {
  //     Animated.timing(fadeAnim, {
  //         toValue: (!toggleSaldo) ? 0 : 1,
  //         duration: 300,
  //         useNativeDriver: true
  //     }).start();

  //     setToggleSaldo(!toggleSaldo);
  // };
  function renderPropostas(){
    var propostas = [
      {"proposta":"123456",
        "valor_emprestimo" : "1500,00",
        "taxa_juros":"1,80",
        "parcelas":"80",
        "valor_parcela":"100",
        "situacao":"1"  
    }
  ]
 var propostasRender =  propostas.map((item)=>{
    return(<Proposta
        proposta={item.proposta}
        valor_emprestimo={item.valor_emprestimo}
        taxa_juros={item.taxa_juros}
        parcelas={item.parcelas}
        valor_parcela={item.valor_parcela}
        situacao={item.situacao}
        />)
      }
      );
  return propostasRender;
    }
  
  
  
  
    useEffect(() => {
    const showCC = shouldViewComponent('contaCorrente');
    setShouldShowContaCorrente(showCC);

    if (showCC) {
      // getSaldo();

      Animated.timing(fadeAnim, {
        toValue: 1,
        duration: 0,
        useNativeDriver: true,
      }).start();
    }
  }, [fadeAnim, isPageFocused]);

  return (
    <SafeAreaView style={{flex: 1}}>
      <StatusBar barStyle="dark-content" hidden backgroundColor={Colors.white} />

      <ScrollView
        style={{
          backgroundColor: Colors.whiteBg,
          // position: Platform.OS === 'ios' ? 'absolute' : 'relative'
        }}
        contentContainerStyle={{flexGrow: 1}}
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl
            colors={[Colors.primary]}
            tintColor={Colors.primary}
            onRefresh={() => {}}
          />
        }>
        <Container>
          <RowLabel>
          <AntIcon name="user" size={25} color={Colors.textPurple} style={{marginRight:5, marginLeft:5}} />
            <LabelTop>Minha Conta</LabelTop>
          </RowLabel>
          <ScrollView   contentContainerStyle={{flexDirection:"row",flexShrink:1,flexWrap:'wrap',justifyContent:"space-evenly"}}> 
          <SquareCard IconName="user" Label="Dados Pessoais" onPress={()=>{
            navigation.navigate('ConfirmarDados')
          }}  IconType='FontAwesome'/>

          <SquareCard IconName="card-account-details" Label="Documentos" IconType="MaterialCommunityIcons" onPress={()=>{
           
            navigation.navigate('EnvioDocumentos');
          }} />
          {/* <SquareCard IconName="bank" Label="Dados Bancários" IconType="FontAwesome"  onPress={()=>{
            navigation.navigate('ConfirmarDados',{step:4});
          }}  />
          <SquareCard IconName="card-account-details" Label="Contato" onPress={()=>{
            navigation.navigate('ConfirmarDados',{step:5});
          }}  IconType="MaterialCommunityIcons" /> */}
            
          </ScrollView>
        </Container>
      </ScrollView>
    </SafeAreaView>
  );
};
