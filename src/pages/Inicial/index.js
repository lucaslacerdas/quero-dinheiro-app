import React, {useState, useEffect, useRef} from 'react';
import {
  ScrollView,
  StatusBar,
  Dimensions,
  SafeAreaView,
  ImageBackground
} from 'react-native';

import {useNavigation, useIsFocused} from '@react-navigation/native';

import Colors from '../../config/colors';
import Button from '../../components/Button';

import fundo from '../../assets/images/bg_Inicial.png';

import {useAuth} from '../../hooks/auth';

import { useAlert } from '../../hooks/Alert';
import {
  Container,
  Header,
  Middle,
  Footer,
  CenterText
} from './styles';



export default function Inicial() {
  const navigation = useNavigation();
  return (
    <ImageBackground source={fundo} style={{height: Dimensions.get('window').height, width:Dimensions.get('window').width, alignItems:'center', justifyContent:'center' }} imageStyle={{resizeMode:"stretch"}} >

    <SafeAreaView style={{flex: 1}}>
      <StatusBar
      hidden
        barStyle="light-content"
        backgroundColor={Colors.backgroundPurple}
      />
      <ScrollView
        style={{
          backgroundColor: 'transparent',
        }}
        contentContainerStyle={{flexGrow: 1}}
        showsVerticalScrollIndicator={false}>
        <Container
          style={{
            backgroundColor: 'transparent',
            alignItems: 'center',
            justifyContent: 'space-around',
          }}>
          <Header>
    
          </Header>
          <Middle>
          <CenterText>
            Simule, compare {'\n'} e contrate. {'\n'} 100% digital. 
          </CenterText>
          </Middle>
          <Footer>
            <Button onPress={()=>navigation.navigate('Register')} backgoundColor={Colors.primary} textColor={Colors.white} style={{marginBottom:20}}>
              Simular
            </Button>
            <Button  onPress={()=>navigation.navigate('Login')} backgoundColor={Colors.primary} textColor={Colors.white}>
              Login
            </Button>
          </Footer>
        </Container>
      </ScrollView>
    </SafeAreaView>
    </ImageBackground>
    
  );
}
