import React, {useState, useEffect, useRef} from 'react';
import {
  ScrollView,
  StatusBar,
  Dimensions,
  SafeAreaView,
  ImageBackground,
} from 'react-native';

import {useNavigation, useIsFocused} from '@react-navigation/native';
import BlockInfo from '../../components/BlockInfo';
import Colors from '../../config/colors';
import Button from '../../components/Button';

import fundo from '../../assets/images/bg_Inicial.png';

import {useAuth} from '../../hooks/auth';

import {useAlert} from '../../hooks/Alert';
import {Container, Texto} from './styles';
import {Card, Row} from 'native-base';

export default function ConfirmarPropostaTexto() {
  const navigation = useNavigation();
  return (
    <SafeAreaView style={{flex: 1}}>
      <StatusBar
        hidden
        barStyle="light-content"
        backgroundColor={Colors.white}
      />
      <ScrollView
        style={{
          backgroundColor: Colors.white,
        }}
        contentContainerStyle={{flexGrow: 1}}
        showsVerticalScrollIndicator={false}>
        <Container
          style={{
            backgroundColor: 'transparent',
            alignItems: 'center',
            justifyContent: 'space-around',
          }}>
          <Texto>
            Em caso de retorno para o saque rescisão, o contrato deverá ser
            liquidado ou somente será possível após o fim dos descontos das
            parcelas. {'\n'} 
            Contrato digitado, em breve chegará um link do Banco, e nele
            terá de cumprir com 4 etapas digitais. O link chegará por SMS. {'\n'} 
            OBS: Agora que digitado o Banco já faz o bloqueio do valor no seu FGTS,
            não se preocupe caso seja cancelado por algum motivo, o desbloqueio
            ocorrerá em instantes.
          </Texto>

          <Button
            backgoundColor={Colors.greenAgree}
            borderColor={Colors.greenAgree}
            onPress={() => {
              navigation.navigate('Home');
            }}
            textColor={Colors.white}
            style={{marginTop: 20, marginBottom: 20}}>
            Eu concordo
          </Button>
        </Container>
      </ScrollView>
    </SafeAreaView>
  );
}
