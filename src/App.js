import React from 'react';
import { StatusBar } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';

import AppProvider from './hooks';
//import linking from './routes/linking';

import Routes from './routes';
import Colors from './config/colors';

export default App = () => {
    return (
        // <NavigationContainer linking={linking}>
        <NavigationContainer >
            <StatusBar barStyle="light-content" backgroundColor={Colors.white} />
            <AppProvider>
                <Routes />
            </AppProvider>
        </NavigationContainer>
    );
};